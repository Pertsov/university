﻿using OfficeOpenXml;
using ScheduleParser.Models;
using System;
using System.IO;
using System.Linq;

namespace ScheduleParser
{
   public static class ScheduleParseProcess
    {
        static ExcelPackage Package { get; set; }
        static ExcelWorksheet Worksheet { get; set; }

        public static void Initialization(string path)
        {
            try
            {
                FileInfo existingFile = new FileInfo(path);

                using (Package = new ExcelPackage(existingFile))
                {
                    // get the first worksheet in the workbook
                    Worksheet = Package.Workbook.Worksheets.FirstOrDefault();
                    var fileParser = new Parser(Worksheet);
                    ScheduleFile.Parse(fileParser);

                }
            }catch(Exception e)
            {
                var s = e;
            }
        }

    }
}
