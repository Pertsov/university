﻿using OfficeOpenXml;
using ScheduleParser.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ScheduleParser
{
    public class Parser
    {
        public ExcelWorksheet Worksheet { get; set; }

        public Parser (ExcelWorksheet w)
        {
            Worksheet = w;
        }

        public List<T> GetDataFromRow<T>(int rowIndex, int columnIndex) where T : BaseCell, new()
        {
            var data = new List<T>();
            for (int i = columnIndex; i < Worksheet.Dimension.Columns; i++)
            {
                var cellRange = Worksheet.Cells[rowIndex, i].GetMergedRangeAddress();
                if (cellRange != null)
                {
                    var cellValue = GetCellParams(rowIndex, i);
                    if (cellValue != null)
                    {
                        var item = new T
                        {
                            TextValue = cellValue.CellValue,
                            IsColored = cellValue.IsColored
                        };
                        GetCellIndexes(ref item, cellRange);
                        data.Add(item);
                    }
                }
            }

            return data;
        }

        public List<T> GetDataFromColumn<T>(int rowIndex, int columnIndex) where T : BaseCell, new()
        {
            var data = new List<T>();
            for (int i = rowIndex; i < Worksheet.Dimension.Rows; i++)
            {
                var cellRange = Worksheet.Cells[i, columnIndex].GetMergedRangeAddress();
                if (cellRange != null)
                {
                    var cellValue = GetCellParams(i, columnIndex);
                    if (cellValue != null)
                    {
                        var item = new T
                        {
                            TextValue = cellValue.CellValue,
                            IsColored = cellValue.IsColored
                        };
                        GetCellIndexes(ref item, cellRange);
                        data.Add(item);
                    }
                }
            }

            return data;
        }

        public List<T> GetDataFromTable<T>(int rowIndex, int columnIndex, int maxRowIndex) where T : BaseCell, new()
        {
            var data = new List<T>();
            for (int i = rowIndex; i < maxRowIndex + 1; i++)
            {
                for (int j = columnIndex; j < Worksheet.Dimension.Columns; j++)
                {
                    var cellRange = Worksheet.Cells[i, j].GetMergedRangeAddress();
                    if (cellRange != null)
                    {
                        var cell = GetCellParams(i, j);
                        if (cell != null)
                        {
                            var item = new T
                            {
                                TextValue = cell.CellValue,
                                IsColored = cell.IsColored
                            };
                            GetCellIndexes(ref item, cellRange);
                            data.Add(item);
                        }
                    }
                }
            }

            return data;
        }

        CellParams GetCellParams(int rowIndex, int columnIndex)
        {
            var cellValue = Worksheet.Cells[rowIndex, columnIndex].Value;
            if (cellValue != null && !string.IsNullOrWhiteSpace(cellValue.ToString()))
            {
                var backgroundColor = Worksheet.Cells[rowIndex, columnIndex].Style.Fill.BackgroundColor.Rgb;
                var result = new CellParams
                {
                    CellValue = cellValue.ToString(),
                    IsColored = !string.IsNullOrWhiteSpace(backgroundColor)
                };
                return result;
            }
            return null;
        }

        public void GetCellIndexes<T>(ref T cell, string cellRange) where T : BaseCell
        {
            var range = cellRange.Split(':');
            if (range.Length > 1)
            {
                cell.StartCellIndex = range[0];
                cell.EndCellIndex = range[1];
            }
            else
            {
                cell.StartCellIndex = cellRange;
                cell.EndCellIndex = cellRange;
            }
        }

    }
    class CellParams
    {
        public string CellValue { get; set; }
        public bool IsColored { get; set; }
    }
}
