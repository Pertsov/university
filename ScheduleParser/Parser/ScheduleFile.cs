﻿using ScheduleParser.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace ScheduleParser
{
    public class GroupedItem
    {
        public string Day { get; set; }
        public string GroupName { get; set; }
        public string SpecialityName { get; set; }
        public string LessonNumber { get; set; }
        public List<LessonCell> Rows { get; set; }
    }

    public class ScheduleView
    {
        public string GroupName { get; set; }
        public string SpecialityName { get; set; }
        public List<LessonCell> Lessons { get; set; }
    }

    public static class ScheduleFile
    {
        static Parser FileParser { get; set; }
        public static List<SpecialityCell> Specialities { get; set; }
        public static List<DayCell> Days { get; set; }
        public static List<WeekCell> Weeks { get; set; }
        public static List<GroupCell> Groups { get; set; }
        public static List<LessonNumberCell> LessonNumbers { get; set; }
        public static List<ScheduleView> Lessons { get; set; }
        public static List<string> Teachers { get; set; }
        public static List<string> Auditories { get; set; }
        public static List<string> Subjects { get; set; }

        static ScheduleFile()
        {
            Specialities = new List<SpecialityCell>();
            Groups = new List<GroupCell>();
            Days = new List<DayCell>();
            Weeks = new List<WeekCell>();
            LessonNumbers = new List<LessonNumberCell>();
            Lessons = new List<ScheduleView>();
            Teachers = new List<string>();
            Auditories = new List<string>();
            Subjects = new List<string>();
        }

        public static void Parse(Parser parser)
        {
            FileParser = parser;
            Specialities = GetSpecialties();
            Groups = GetGroups();
            ConnectGroupToSpecialities();
            Days = GetDays();
            Weeks = GetWeeks();
            LessonNumbers = GetLessonNumberCells();
            Lessons = GetLessons();
        }

        public static List<GroupCell> GetGroups()
        {
            return FileParser.GetDataFromRow<GroupCell>((int)ConstantIndexes.GroupRowIndex, (int)ConstantIndexes.ContentColumnIndex)
                   .Select(s => new GroupCell
                   {
                       StartCellIndex = s.StartCellIndex,
                       EndCellIndex = s.EndCellIndex,
                       GroupName = s.TextValue,
                   }).ToList();
        }

        public static List<SpecialityCell> GetSpecialties()
        {
            return FileParser.GetDataFromRow<SpecialityCell>((int)ConstantIndexes.SpecialityRowIndex, (int)ConstantIndexes.ContentColumnIndex)
                   .Select(s => new SpecialityCell
                   {
                       StartCellIndex = s.StartCellIndex,
                       EndCellIndex = s.EndCellIndex,
                       Speciality = s.TextValue
                   }).ToList();
        }

        public static List<DayCell> GetDays()
        {
            return FileParser.GetDataFromColumn<DayCell>((int)ConstantIndexes.DayRowIndex, (int)ConstantIndexes.DayColumnIndex)
                   .Select(s => new DayCell
                   {
                       StartCellIndex = s.StartCellIndex,
                       EndCellIndex = s.EndCellIndex,
                       Day = s.TextValue
                   }).ToList();
        }

        public static List<LessonNumberCell> GetLessonNumberCells()
        {
            return FileParser.GetDataFromColumn<LessonNumberCell>((int)ConstantIndexes.LessonNumberRowIndex, (int)ConstantIndexes.LessonNumberColumIndex)
                   .Select(s => new LessonNumberCell
                   {
                       StartCellIndex = s.StartCellIndex,
                       EndCellIndex = s.EndCellIndex,
                       LessonNumber = s.TextValue
                   }).ToList();
        }

        public static List<WeekCell> GetWeeks()
        {
            return FileParser.GetDataFromColumn<WeekCell>((int)ConstantIndexes.WeekRowIndex, (int)ConstantIndexes.WeekColumnIndex)
                   .Select(s => new WeekCell
                   {
                       StartCellIndex = s.StartCellIndex,
                       EndCellIndex = s.EndCellIndex,
                       WeekName = s.TextValue
                   }).ToList();
        }

        public static List<ScheduleView> GetLessons()
        {
            var list = FileParser.GetDataFromTable<LessonCell>((int)ConstantIndexes.ContentRowIndex, (int)ConstantIndexes.ContentColumnIndex, Days.Last().EndCellIndex.GetNumber());
            var resultList = new List<LessonCell>();
            foreach (var item in list)
            {
                var itemGroups = Groups.Where(g => g.IsInColumnRange(item.StartCellIndex, item.EndCellIndex)).ToList();
                if (itemGroups.Count != 0) // it's need to be changed
                {
                    item.Specialities = itemGroups.Select(s => s.SpecialityName).Distinct().ToList();
                    item.Groups = itemGroups;
                    item.Day = Days.Where(d => d.IsInRowRange(item.StartCellIndex, item.EndCellIndex)).Select(d => d.Day).FirstOrDefault();
                    item.LessonNumber = LessonNumbers.Where(l => l.IsInRowRange(item.StartCellIndex, item.EndCellIndex)).Select(s => s.LessonNumber).FirstOrDefault();
                    if (!item.IsMergedRows)
                    {
                        item.Week = Weeks.Where(w => w.IsInRowRange(item.StartCellIndex, item.EndCellIndex)).Select(w => w.WeekName).FirstOrDefault();
                    }
                    resultList.Add(item);
                }

            }
            var _list = new List<ScheduleView>();
            foreach (var item in Groups)
            {
                var rows = resultList.Where(r => r.Groups.Contains(item));
                
                var groupedItems = rows.GroupBy(d => new { d.LessonNumber, item.GroupName, d.Day, item.SpecialityName },
                    (key, group) => new GroupedItem
                    {
                        Day = key.Day,
                        LessonNumber = key.LessonNumber,
                        GroupName = key.GroupName,
                        SpecialityName = key.SpecialityName,
                        Rows = group.ToList()
                    }).ToList();

                var groupLessons = ParseRows(groupedItems).ToList();
                var scheduleItem = new ScheduleView
                {
                    SpecialityName = item.SpecialityName,
                    GroupName = item.GroupName,
                    Lessons = groupLessons
                };
                _list.Add(scheduleItem);
            }

            return _list;
        }

        static List<LessonCell> ParseRows(List<GroupedItem> groupedItems)
        {
            var result = new List<LessonCell>();
            foreach (var item in groupedItems)
            {
                var info = new LessonCell();

                switch (item.Rows.Count)
                {
                    case 1: // Fizra
                        {
                            info.Day = item.Day;
                            info.Groups = item.Rows.First().Groups;
                            info.LessonNumber = item.Rows.First().LessonNumber;
                            info.Lesson = GetLesson(item.Rows);
                            //Do we need that for fizra ?
                            info.Type = item.Rows.First().IsColored ? "LECTURE" : "PRACTICAL";
                            info.Specialities = item.Rows.First().Specialities;
                            info.Week = item.Rows.First().Week;
                            result.Add(info);
                            if (!Subjects.Contains(info.Lesson))
                            {
                                Subjects.Add(info.Lesson);
                            }
                            break;
                        }
                    case 2:
                        {
                            info.Day = item.Day;
                            info.Groups = item.Rows.First().Groups;
                            info.LessonNumber = item.Rows.First().LessonNumber;
                            info.Lesson = GetLesson(item.Rows);
                            info.Type = item.Rows.First().IsColored ? "LECTURE" : "PRACTICAL";
							if (Regex.IsMatch(item.Rows[1].TextValue, "[0-9]"))
                            {
                                info.Auditory = GetAuditory(item.Rows, 1);
                                if (!Auditories.Contains(info.Auditory))
                                {
                                    Auditories.Add(info.Auditory);
                                }
                            }
                            else
                            {
                                info.Teacher = GetTeacher(item.Rows, 1);
                                if (!Teachers.Contains(info.Teacher))
                                {
                                    Teachers.Add(info.Teacher);
                                }
                            }
                            info.Week = item.Rows.First().Week;
                            info.Specialities = item.Rows.First().Specialities;
                            result.Add(info);

                            if (!Subjects.Contains(info.Lesson))
                            {
                                Subjects.Add(info.Lesson);
                            }

                            break;
                        }
                    case 3:
                        {
                            info = MapLesson(item);
                            result.Add(info);
                            break;
                        }
                    case 4: // groups with the same name there are the same
                        {
                            info.Day = item.Day;
                            info.Groups = item.Rows.First().Groups;
                            info.LessonNumber = item.Rows.First().LessonNumber;
                            info.Lesson = GetLesson(item.Rows);
                            info.Type = item.Rows.First().IsColored ? "LECTURE" : "PRACTICAL";
							if (Regex.IsMatch(item.Rows[1].TextValue, "[0-9]"))
                            {
                                info.Auditory = GetAuditory(item.Rows, 1);
                                if (!Auditories.Contains(info.Auditory))
                                {
                                    Auditories.Add(info.Auditory);
                                }
                            }
                            else
                            {
                                info.Teacher = GetTeacher(item.Rows, 1);
                                if (!Teachers.Contains(info.Teacher))
                                {
                                    Teachers.Add(info.Teacher);
                                }
                            }
                            info.Week = item.Rows.First().Week;
                            info.Specialities = item.Rows.First().Specialities;
                            result.Add(info);

                            if (!Subjects.Contains(info.Lesson))
                            {
                                Subjects.Add(info.Lesson);
                            }
                            break;
                        }
                    case 5: // 3 and 2 or visa versa
                        {
                            info.Day = item.Day;
                            info.Groups = item.Rows.First().Groups;
                            info.LessonNumber = item.Rows.First().LessonNumber;
                            info.Specialities = item.Rows.First().Specialities;

                            if (Regex.IsMatch(item.Rows[1].TextValue, "[0-9]"))
                            {
                                info.Lesson = GetLesson(item.Rows);
                                info.Week = item.Rows.First().Week;
                                info.Auditory = GetAuditory(item.Rows, 1);
                                result.Add(info);
                                info = MapLesson(item, 2);
                                result.Add(info);
                            }
                            else
                            {
                                if (Regex.IsMatch(item.Rows[2].TextValue, "[0-9]"))
                                {
                                    info = MapLesson(item);
                                    result.Add(info);
                                    info.Lesson = GetLesson(item.Rows, 3);
                                    info.Week = item.Rows[3].Week;
                                    info.Type = item.Rows[3].IsColored ? "LECTURE" : "PRACTICAL";

									if (Regex.IsMatch(item.Rows[4].TextValue, "[0-9]"))
                                    {
                                        info.Auditory = GetAuditory(item.Rows, 4);
                                        info.Teacher = null;
                                    }
                                    else
                                    {
                                        info.Teacher = GetTeacher(item.Rows, 4);
                                        info.Auditory = null;
                                    }
                                    result.Add(info);
                                }
                                else
                                {
                                    info.Lesson = GetLesson(item.Rows);
                                    info.Week = item.Rows.First().Week;
                                    info.Type = item.Rows.First().IsColored ? "LECTURE" : "PRACTICAL";
									info.Teacher = GetTeacher(item.Rows);
                                    result.Add(info);
                                    info = MapLesson(item, 2);
                                    result.Add(info);
                                }

                            }

                            break;
                        }
                    case 6:
                        {
                            info = MapLesson(item);
                            result.Add(info);
                            info = MapLesson(item, 3);
                            result.Add(info);
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
            }
            return result;
        }

        public static void ConnectGroupToSpecialities()
        {
            foreach (var speciality in Specialities)
            {
                speciality.Groups = new List<string>();
                foreach (var group in Groups)
                {
                    var start = Helpers.GetColumnOrder(speciality.StartCellIndex, group.StartCellIndex);
                    var end = Helpers.GetColumnOrder(speciality.EndCellIndex, group.EndCellIndex);
                    if (start < 1 && end > -1)
                    {
                        speciality.Groups.Add(group.GroupName);
                        group.SpecialityName = speciality.Speciality;
                    }
                }
            }
        }

        public static string GetLesson(IEnumerable<LessonCell> items, int itemIndex = 0) => GetEntity(items, itemIndex);
        public static string GetTeacher(IEnumerable<LessonCell> items, int itemIndex = 1) => GetEntity(items, itemIndex);
        public static string GetAuditory(IEnumerable<LessonCell> items, int itemIndex = 2) => GetEntity(items, itemIndex);

        static string GetEntity(IEnumerable<LessonCell> items, int itemIndex)
        {
            return items.ToList()[itemIndex].TextValue;
        }

        static LessonCell MapLesson(GroupedItem item, int skipNumber = 0)
        {
            var cellItem = new LessonCell
            {
                Day = item.Day,
                Groups = item.Rows.First().Groups,
                Specialities = item.Rows.First().Specialities,
                LessonNumber = item.LessonNumber,
                Lesson = GetLesson(item.Rows.Skip(skipNumber)),
                Type = item.Rows.First().IsColored ? "LECTURE" : "PRACTICAL",
				Teacher = GetTeacher(item.Rows.Skip(skipNumber)),
                Auditory = GetAuditory(item.Rows.Skip(skipNumber)),
                Week = item.Rows.First().Week,
            };
            if (!Teachers.Contains(cellItem.Teacher))
            {
                Teachers.Add(cellItem.Teacher);
            }
            if (!Auditories.Contains(cellItem.Auditory))
            {
                Auditories.Add(cellItem.Auditory);
            }
            if (!Subjects.Contains(cellItem.Lesson))
            {
                Subjects.Add(cellItem.Lesson);
            }
            return cellItem;
        }
    }
}
