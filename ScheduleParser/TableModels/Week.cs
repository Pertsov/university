﻿using ScheduleParser.Models;
using System;

namespace ParseSchedule.TableModels
{
    public class Week : ITableModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
