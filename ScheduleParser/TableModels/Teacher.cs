﻿using System;
using ScheduleParser.Models;

namespace ParseSchedule.TableModels
{
   public class Teacher : ITableModel
    {
        public string Name { get; set; }
    }
}
