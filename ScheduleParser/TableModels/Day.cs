﻿using System;

namespace ParseSchedule.TableModels
{
   public class Day
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
