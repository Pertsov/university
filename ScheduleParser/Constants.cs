﻿namespace ScheduleParser
{
    enum ConstantIndexes
    {
        DayColumnIndex = 1,
        ContentRowIndex = 6,
        ContentColumnIndex = 4,
        SpecialityRowIndex = 4,
        GroupRowIndex = 5,
        DayRowIndex = 6,
        LessonNumberRowIndex = 6,
        LessonNumberColumIndex = 2,
        WeekRowIndex = 6,
        WeekColumnIndex = 3,
    }
}
