﻿namespace ScheduleParser.Models
{
    public class GroupCell : BaseCell
    {
        public string GroupName { get; set; }
        public string SpecialityName { get; set; }
    }
}
