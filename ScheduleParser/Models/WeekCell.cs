﻿using ParseSchedule.TableModels;

namespace ScheduleParser.Models
{
    public class WeekCell : BaseCell
    {
        public string WeekName { get; set; }
    }
}
