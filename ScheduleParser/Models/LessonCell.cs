﻿using ParseSchedule.TableModels;
using System.Collections.Generic;

namespace ScheduleParser.Models
{
   public class LessonCell : BaseCell
    {
        public string Day { get; set; }
        public string Lesson { get; set; }
        public List<GroupCell> Groups { get; set; }
        public List<string> Specialities { get; set; }
        public string Teacher { get; set; }
        public string LessonNumber { get; set; }
        public string Auditory { get; set; }
        public string Week { get; set; }
        public string Type { get; set; }
    }
}
