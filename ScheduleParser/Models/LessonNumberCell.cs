﻿namespace ScheduleParser.Models
{
    public class LessonNumberCell : BaseCell
    {
        public string LessonNumber { get; set; }
    }
}
