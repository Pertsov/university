﻿using System.Collections.Generic;

namespace ScheduleParser.Models
{
    public class SpecialityCell : BaseCell
    {
        public string Speciality { get; set; }
        public List<string> Groups { get; set; }
    }
}
