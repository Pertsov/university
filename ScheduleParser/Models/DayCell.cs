﻿using ParseSchedule.TableModels;
using System.Collections.Generic;

namespace ScheduleParser.Models
{
   public class DayCell : BaseCell
    {
        public string Day { get; set; }
        public List<LessonNumberCell> LessonNumbers { get; set; }
    }
}
