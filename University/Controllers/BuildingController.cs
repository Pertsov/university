﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using University.Core.DAL;
using University.Core.DAL.Models;

namespace University.Controllers
{
	[Produces("application/json")]
	[Route("api/[controller]")]
    [ApiController]
	[Authorize]
	public class BuildingController : ControllerBase 
    {
		private readonly IRepository<Building> buildingRepository;
		private readonly ILogger<BuildingController> _logger;

		public BuildingController(IRepository<Building> buildingRepository, ILogger<BuildingController> logger)
		{
			this.buildingRepository = buildingRepository;
			this._logger = logger;
		}

		[HttpGet]
		[Route("GetAll")]
		public async Task<IEnumerable<Building>> GetAll()
		{
			_logger.LogTrace("Building GetAll", LogLevel.Trace);
			return await buildingRepository.GetAll();
		}

		[HttpGet]
		[Route("GetById")]
		public async Task<Building> GetById(int id)
		{
			return await buildingRepository.GetById(id);
		}

		[HttpPost]
		[Route("Delete")]
		[Authorize(Roles = "Administrator")]
		public async void Delete(Building building)
		{
			await buildingRepository.Delete(building);
		}

		[HttpPost]
		[Route("Add")]
		[Authorize(Roles = "Administrator")]
		public async Task<ActionResult> Add(Building building)
		{
			return Ok(await buildingRepository.Insert(building));
		}

		[HttpPost]
		[Route("Update")]
		[Authorize(Roles = "Administrator")]
		public async Task<ActionResult> Update(Building building)
		{
			return Ok(await buildingRepository.Update(building));
		}
	}
}
