﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using University.Core.DAL;
using University.Core.DAL.Models;
using University.ViewModel;

namespace University.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
	[Authorize]
	public class StudentController : ControllerBase
    { 
		private readonly IRepository<Student> _studentRepository;
		private readonly ILogger<StudentController> _logger;

		public StudentController(IRepository<Student> studentRepository, ILogger<StudentController> logger)
		{
			this._studentRepository = studentRepository;
			this._logger = logger;
		}

		[HttpGet]
		[Route("GetAll")]
		public async Task<IEnumerable<Student>> GetAll()
		{
			return await _studentRepository.GetAll();
		}

		[HttpGet]
		[Route("GetById")]
		public async Task<IActionResult> GetById(int id)
		{
			return Ok(await _studentRepository.GetById(id));
		}

		[HttpPost]
		[Route("Add")]
		[Authorize(Roles = "Administrator")]
		public async Task<ActionResult> Add(Student student)
		{
			return Ok(await _studentRepository.Insert(student));
		}

		[HttpPost]
		[Route("Update")]
		[Authorize(Roles = "Administrator")]
		public async Task<ActionResult> Update(Student student)
		{
			return Ok(await _studentRepository.Update(student));
		}

		[HttpPost]
		[Route("Delete")]
		[Authorize(Roles = "Administrator")]
		public async void Delete(Student student)
		{
			await _studentRepository.Delete(student);
		}
	}
} 