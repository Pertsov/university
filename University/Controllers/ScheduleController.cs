﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using University.Core.DAL;
using University.Core.DAL.Models;
using University.Core.DAL.Repositories;
using University.ViewModel;

namespace University.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
	[Authorize]
	public class ScheduleController : ControllerBase
    {
		private readonly IUserRepository _userRepository;
		private readonly IRepository<Schedule> _scheduleRepository;
        private readonly ILogger<ScheduleController> _logger;
        private readonly DataContext _context;
		private readonly IHostingEnvironment _appEnvironment;
		private readonly UserManager<IdentityUser> _userManager;

		public ScheduleController(DataContext context, IRepository<Schedule> scheduleRepository, ILogger<ScheduleController> logger, IHostingEnvironment appEnvironment,
			UserManager<IdentityUser> userManager, IUserRepository userRepository)
		{
			_logger = logger;
            _context = context;
            _appEnvironment = appEnvironment;
            _scheduleRepository = scheduleRepository;
			_userManager = userManager;
			_userRepository = userRepository;
        }
		[HttpPost]
		[Route("GetAll")]
		public async Task<IEnumerable<ScheduleView>> GetAll(ScheduleFilter filter)
		{
			var schedules = await _scheduleRepository.Entities
													.Include(s => s.Subject)
													.Include(s => s.Teacher)
													.Include(s => s.Group)
													.Include(s => s.ScheduleType)
													.Include(s => s.Auditory).ThenInclude(a => a.Building)
													.Where(s => (filter.AuditoryId == null || s.AuditoryId == filter.AuditoryId) &&
															(filter.GroupId == null || s.GroupId == filter.GroupId) &&
															(filter.TeacherId == null || s.TeacherId == filter.TeacherId) &&
															(filter.SubjectId == null || s.SubjectId == filter.SubjectId) &&
															(filter.End == null || s.Date.Date >= filter.Start && s.Date.Date <= filter.End) &&
															(filter.End != null || s.Date.Date == filter.Start.Date)).ToListAsync();

			return schedules.Select(s => Mapper.Map<Schedule, ScheduleView>(s));
		}

		[HttpGet]
		[Route("GetAll")]
		public async Task<IEnumerable<ScheduleView>> GetAll(DateTime start, DateTime? end)
		{
			var student = await _userRepository.GetStudentInfo(User.Identity.Name);
			var teacher = await _userRepository.GetTeacherInfo(User.Identity.Name);
			if (student == null && teacher == null) return null;

			var schedules = await _scheduleRepository.Entities
				.Include(s => s.Subject)
				.Include(s => s.Group)
				.Include(s => s.Teacher)
				.Include(s => s.ScheduleType)
				.Include(s => s.Auditory).ThenInclude(a => a.Building)
				.Where(s => (student == null || s.GroupId == student.GroupId) &&
							(teacher == null || s.TeacherId == teacher.Id) &&
							(end == null || s.Date >= start && s.Date <= end) &&
							(end != null || s.Date == start)).ToListAsync();
			
			return schedules.Select(s => Mapper.Map<Schedule, ScheduleView>(s));
		}

		[HttpGet]
		[Route("GetById")]
		public async Task<IActionResult> GetById(int id)
		{
			var schedule = await _scheduleRepository.Entities
				.Include(s => s.Subject)
				.Include(s => s.Teacher)
				.Include(s => s.ScheduleType)
				.Include(s => s.Notes)
				.Include(s => s.Group).ThenInclude(st => st.Students)
				.Include(s => s.Auditory).ThenInclude(a => a.Building)
				.Include(s => s.Attendances).ThenInclude(a => a.Student).ThenInclude(g => g.Group)
				.SingleOrDefaultAsync(s => s.Id == id);

			if (schedule == null) return NotFound();

			return Ok(Mapper.Map<Schedule, ScheduleDetailsView>(schedule));
		}

		[HttpPost]
		[Route("Add")]
		[Authorize(Roles = "Administrator")]
		public async Task<ActionResult> Add(Schedule schedule)
		{
			return Ok(await _scheduleRepository.Insert(schedule));
		}

		[HttpPost]
		[Route("Update")]
		[Authorize(Roles = "Administrator, Teacher")]
		public async Task<ActionResult> Update(Schedule schedule)
		{
			return Ok(await _scheduleRepository.Update(schedule));
		}

		[HttpPost]
		[Route("Delete")]
		[Authorize(Roles = "Administrator")]
		public async void Delete(Schedule schedule)
		{
			await _scheduleRepository.Delete(schedule);
		}
	}
}