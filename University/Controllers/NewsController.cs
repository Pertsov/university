﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using University.Core.DAL;
using University.Core.DAL.Models;
using University.ViewModel;

namespace University.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
	[Authorize]
	public class NewsController : ControllerBase
    {
		private readonly IRepository<News> _newsRepository;
		private readonly ILogger<NewsController> _logger;
		private readonly IHostingEnvironment _appEnvironment;

		public NewsController(IRepository<News> newsRepository, ILogger<NewsController> logger, IHostingEnvironment appEnvironment)
		{
			_newsRepository = newsRepository;
			_logger = logger;
			_appEnvironment =  appEnvironment;
		}

		[HttpGet]
		[Route("GetAll")]
		public async Task<IEnumerable<News>> GetAll()
		{
			return await _newsRepository.GetAll();
		}

		[HttpGet]
		[Route("GetById")]
		public async Task<IActionResult> GetById(int id)
		{
			return Ok(await _newsRepository.GetById(id));
		}

		[HttpGet]
		[Route("GetImage")]
		public async Task<IActionResult> GetImage(int newsId)
		{
			var news = await _newsRepository.Entities.SingleOrDefaultAsync(s => s.Id == newsId);

			if (news == null) return NotFound();
			string path = _appEnvironment.WebRootPath + "\\Files\\";
			byte[] b = System.IO.File.ReadAllBytes(path + news.Image);
			return File(b, news.ContentType);
		}

		[HttpPost]
		[Route("Add")]
		[Authorize(Roles = "Administrator, Teacher")]
		public async Task<IActionResult> Add(News news)
		{
			news.ApplicationUserId = User.Identity.Name;

			return Ok(await _newsRepository.Insert(news));
		}
      

		[HttpPost]
		[Route("AddImage")]
		[Authorize(Roles = "Administrator, Teacher")]
		public async Task<IActionResult> AddImage(IFormFile img, int newsId)
		{
			var news = await _newsRepository.Entities.SingleOrDefaultAsync(s => s.Id == newsId);

			if (news == null) return NotFound();

			string path = _appEnvironment.WebRootPath + "\\Files\\";
			bool exists = System.IO.Directory.Exists(path);

			if (!exists)
				System.IO.Directory.CreateDirectory(path);

			var imageName = Guid.NewGuid();

			if (img.Length > 0)
			{
				try
				{
					using (var stream = new FileStream(path + imageName.ToString(), FileMode.Create))
					{
						img.CopyTo(stream);
					}
					
					news.Image = imageName;
					news.ContentType = img.ContentType; 
					return Ok(await _newsRepository.Update(news));
				}
				catch (Exception e)
				{
					return BadRequest(e);
				}
			}
			return BadRequest();
		}


		[HttpPost]
		[Route("Update")]
		[Authorize(Roles = "Administrator, Teacher")]
		public async Task<IActionResult> Update(News news)
		{
			return Ok(await _newsRepository.Update(news));
		}

		[HttpPost]
		[Route("Delete")]
		[Authorize(Roles = "Administrator")]
		public async void Delete(News news)
		{
			await _newsRepository.Delete(news);
		}

	}
}