﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using University.ViewModel;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;

namespace CustomIdentityApp.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[Authorize(Roles = "Administrator")]
	public class UserController : Controller
	{
		UserManager<IdentityUser> _userManager;

		public UserController(UserManager<IdentityUser> userManager)
		{
			_userManager = userManager;
		}

		[HttpGet]
		[Route("GetAll")]
		public IEnumerable<UserView> GetAllUsers()
		{
			var users = _userManager.Users.ToList();
			return users.Select(s => Mapper.Map<IdentityUser, UserView>(s));
		}
		
		[HttpPost]
		[Route("Add")]
		public async Task<IActionResult> Create(CreateUserViewModel model)
		{
			if (ModelState.IsValid)
			{
				IdentityUser user = new IdentityUser { Email = model.Email, UserName = model.Email };
				var result = await _userManager.CreateAsync(user, model.Password);
				if (result.Succeeded)
				{
					return Ok(user);
				}
				else
				{
					foreach (var error in result.Errors)
					{
						ModelState.AddModelError(string.Empty, error.Description);
					}
				}
			}
			return BadRequest(model);
		}

		[HttpGet]
		[Route("GetById")]
		public async Task<IActionResult> GetUserById(string id)
		{
			IdentityUser user = await _userManager.FindByIdAsync(id);
			if (user == null)
			{
				return NotFound();
			}
			EditUserViewModel model = new EditUserViewModel { Id = user.Id, Email = user.Email };
			return Ok(model);
		}

		[HttpPost]
		[Route("Update")]
		public async Task<IActionResult> Edit(EditUserViewModel model)
		{
			if (ModelState.IsValid)
			{
				IdentityUser user = await _userManager.FindByIdAsync(model.Id);
				if (user != null)
				{
					user.Email = model.Email;
					user.UserName = model.Email;

					var result = await _userManager.UpdateAsync(user);
					if (result.Succeeded)
					{
						return Ok(model);
					}
					else
					{
						foreach (var error in result.Errors)
						{
							ModelState.AddModelError(string.Empty, error.Description);
						}
					}
				}
			}
			return Ok(model);
		}

		[HttpPost]
		[Route("Delete")]
		public async Task<IActionResult> Delete(string id)
		{
			IdentityUser user = await _userManager.FindByIdAsync(id);
			if (user != null)
			{
				IdentityResult result = await _userManager.DeleteAsync(user);
			}
			return Ok();
		}


		[HttpPost]
		[Route("ChangePassword")]
		public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
		{
			if (ModelState.IsValid)
			{
				IdentityUser user = await _userManager.FindByIdAsync(model.Id);
				if (user != null)
				{
					IdentityResult result =
						await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
					if (result.Succeeded)
					{
						return Ok(model);
					}
					else
					{
						foreach (var error in result.Errors)
						{
							ModelState.AddModelError(string.Empty, error.Description);
						}
					}
				}
				else
				{
					return NotFound("User not found");
				}
			}
			return BadRequest(model);
		}
	}
}