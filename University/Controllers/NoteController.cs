﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using University.Core.DAL;
using University.Core.DAL.Models;

namespace University.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
	[Authorize]
	public class NoteController : ControllerBase
    {
		private readonly IRepository<Note> noteRepository;
		private readonly ILogger<NoteController> _logger;

		public NoteController(IRepository<Note> noteRepository, ILogger<NoteController> logger)
		{
			this.noteRepository = noteRepository;
			this._logger = logger;
		}

		[HttpGet]
		[Route("GetAll")]
		public async Task<IEnumerable<Note>> GetAll()
		{
			return await noteRepository.GetAll();
		}

		[HttpGet]
		[Route("GetById")]
		public async Task<Note> GetById(int id)
		{
			return await noteRepository.GetById(id);
		}

		[HttpPost]
		[Route("Add")]
		public async Task<ActionResult> Add(Note note)
		{
			return Ok(await noteRepository.Insert(note));
		}

		[HttpPost]
		[Route("Update")]
		public async Task<ActionResult> Update(Note note)
		{
			return Ok(await noteRepository.Update(note));
		}

		[HttpPost]
		[Route("Delete")]
		public async void Delete(Note note)
		{
			await noteRepository.Delete(note);
		}
	}
}