﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using University.Core.DAL.Helpers;
using University.Core.DAL.Repositories;
using University.ViewModel;

namespace University.Controllers
{
	[Route("[controller]/[action]")]
	public class AccountController : Controller
	{
		private readonly SignInManager<IdentityUser> _signInManager;
		private readonly UserManager<IdentityUser> _userManager;
		private readonly IUserRepository _userRepository;

		public AccountController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, IUserRepository userRepository)
		{
			_userManager = userManager;
			_signInManager = signInManager;
			_userRepository = userRepository;
		}

		[HttpPost]
		[AllowAnonymous]
		public async Task<object> Login([FromBody] LoginDto model)
		{
			var user = await _userManager.FindByEmailAsync(model.Email);

			var result = await _signInManager.PasswordSignInAsync(user, model.Password, false, false);

			if (result.Succeeded)
			{
				var appUser = _userManager.Users.SingleOrDefault(r => r.Email == model.Email);
				return await _userRepository.GenerateJwtToken(model.Email, appUser);
			}
			return BadRequest("Invalid credential");
		}

		[HttpPost]
		[Authorize]
		public async Task<object> GetUserInfo()
		{
			var teacher = await _userRepository.GetTeacherInfo(User.Identity.Name);
			if (teacher != null) return teacher;
			return await _userRepository.GetStudentInfo(User.Identity.Name);

		}

		[HttpPost]
		[Authorize]
		public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
		{
			if (model.Id != User.Identity.Name) return NotFound("User not found");
			if (ModelState.IsValid)
			{
				IdentityUser user = await _userManager.FindByIdAsync(model.Id);
				if (user != null)
				{
					IdentityResult result =
						await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
					if (result.Succeeded)
					{
						return Ok(model);
					}
					else
					{
						foreach (var error in result.Errors)
						{
							ModelState.AddModelError(string.Empty, error.Description);
						}
					}
				}
				else
				{
					return NotFound("User not found");
				}
			}
			return BadRequest(model);
		}

	}
}