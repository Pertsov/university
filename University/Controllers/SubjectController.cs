﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using University.Core.DAL;
using University.Core.DAL.Models;

namespace University.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
	[Authorize]
	public class SubjectController : ControllerBase
    {
		private readonly IRepository<Subject> subjectRepository;
		private readonly ILogger<SubjectController> _logger;

		public SubjectController(IRepository<Subject> subjectRepository, ILogger<SubjectController> logger)
		{
			this.subjectRepository = subjectRepository;
			this._logger = logger;
		}

		[HttpGet]
		[Route("GetAll")]
		public async Task<IEnumerable<Subject>> GetAll()
		{
			return await subjectRepository.GetAll();
		}

		[HttpGet]
		[Route("GetById")]
		public async Task<Subject> GetById(int id)
		{
			return await subjectRepository.GetById(id);
		}

		[HttpPost]
		[Route("Add")]
		[Authorize(Roles = "Administrator")]
		public async Task<ActionResult> Add(Subject subject)
		{
			return Ok(await subjectRepository.Insert(subject));
		}

		[HttpPost]
		[Route("Update")]
		[Authorize(Roles = "Administrator")]
		public async Task<ActionResult> Update(Subject subject)
		{
			return Ok(await subjectRepository.Update(subject));
		}

		[HttpPost]
		[Route("Delete")]
		[Authorize(Roles = "Administrator")]
		public async void Delete(Subject subject)
		{
			await subjectRepository.Delete(subject);
		}
	}
}