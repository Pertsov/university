﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using University.Core.DAL;
using University.Core.DAL.Models;

namespace University.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
	[Authorize]
	public class FacultyController : ControllerBase
    {
		private readonly IRepository<Faculty> facultyRepository;
		private readonly ILogger<FacultyController> _logger;

		public FacultyController(IRepository<Faculty> facultyRepository, ILogger<FacultyController> logger)
		{
			this.facultyRepository = facultyRepository;
			this._logger = logger;
		}

		[HttpGet]
		[Route("GetAll")]
		public async Task<IEnumerable<Faculty>> GetAll()
		{
			return await facultyRepository.GetAll();
		}

		[HttpGet]
		[Route("GetById")]
		public async Task<Faculty> GetById(int id)
		{
			return await facultyRepository.GetById(id);
		}

		[HttpPost]
		[Route("Add")]
		[Authorize(Roles = "Administrator")]
		public async Task<ActionResult> Add(Faculty faculty)
		{
			return Ok(await facultyRepository.Insert(faculty));
		}

		[HttpPost]
		[Route("Update")]
		[Authorize(Roles = "Administrator")]
		public async Task<ActionResult> Update(Faculty faculty)
		{
			return Ok(await facultyRepository.Update(faculty));
		}

		[HttpPost]
		[Route("Delete")]
		[Authorize(Roles = "Administrator")]
		public async void Delete(Faculty faculty)
		{
			await facultyRepository.Delete(faculty);
		}
	}
}