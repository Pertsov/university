﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using University.Core.DAL;
using University.Core.DAL.Models;
using University.ViewModel;

namespace University.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
	[Authorize]
	public class SpecialtyController : ControllerBase
    { 
		private readonly IRepository<Specialty> specialtyRepository;
		private readonly ILogger<SpecialtyController> _logger;

		public SpecialtyController(IRepository<Specialty> specialtyRepository, ILogger<SpecialtyController> logger)
		{
			this.specialtyRepository = specialtyRepository;
			this._logger = logger;
		}

		[HttpGet]
		[Route("GetAll")]
		public async Task<IEnumerable<SpecialtyView>> GetAll()
		{
			var specialtes = await specialtyRepository.Entities
				.Include(s => s.Faculty).ToListAsync();

			return specialtes.Select(s => Mapper.Map<Specialty, SpecialtyView>(s));
		}

		[HttpGet]
		[Route("GetById")]
		public async Task<IActionResult> GetById(int id)
		{
			var specialty = await specialtyRepository.Entities
				.Include(s => s.Faculty).SingleOrDefaultAsync(s => s.Id == id);

			if (specialty == null) return NotFound();

			return Ok(specialty);
		}

		[HttpPost]
		[Route("Add")]
		[Authorize(Roles = "Administrator")]
		public async Task<ActionResult> Add(Specialty specialty)
		{
			return Ok(await specialtyRepository.Insert(specialty));
		}

		[HttpPost]
		[Route("Update")]
		[Authorize(Roles = "Administrator")]
		public async Task<ActionResult> Update(Specialty specialty)
		{
			return Ok(await specialtyRepository.Update(specialty));
		}

		[HttpPost]
		[Route("Delete")]
		[Authorize(Roles = "Administrator")]
		public async void Delete(Specialty specialty)
		{
			await specialtyRepository.Delete(specialty);
		}
	}
} 