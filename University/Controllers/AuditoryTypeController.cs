﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using University.Core.DAL;
using University.Core.DAL.Models;

namespace University.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
	[Authorize]
	public class AuditoryTypeController : ControllerBase
    {
		private readonly IRepository<AuditoryType> auditoryTypeRepository;
		private readonly ILogger<AuditoryTypeController> _logger;

		public AuditoryTypeController(IRepository<AuditoryType> auditoryTypeRepository, ILogger<AuditoryTypeController> logger)
		{
			this.auditoryTypeRepository = auditoryTypeRepository;
			this._logger = logger;
		}

		[HttpGet]
		[Route("GetAll")]
		public async Task<IEnumerable<AuditoryType>> GetAll()
		{
			return await auditoryTypeRepository.GetAll();
		}

		[HttpGet]
		[Route("GetById")]
		public async Task<AuditoryType> GetById(int id)
		{
			return await auditoryTypeRepository.GetById(id);
		}
		
		[HttpPost]
		[Route("Add")]
		[Authorize(Roles = "Administrator")]
		public async Task<ActionResult> Add(AuditoryType auditoryType)
		{
			return Ok(await auditoryTypeRepository.Insert(auditoryType));
		}

		[HttpPost]
		[Route("Update")]
		[Authorize(Roles = "Administrator")]
		public async Task<ActionResult> Update(AuditoryType auditoryType)
		{
			return Ok(await auditoryTypeRepository.Update(auditoryType));
		}

		[HttpPost]
		[Route("Delete")]
		[Authorize(Roles = "Administrator")]
		public async void Delete(AuditoryType auditoryType)
		{
			await auditoryTypeRepository.Delete(auditoryType);
		}
	}
}