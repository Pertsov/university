﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using University.Core.DAL;
using University.Core.DAL.Models;

namespace University.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
	[Authorize]
	public class GroupController : ControllerBase
    {
		private readonly IRepository<Group> groupRepository;
		private readonly ILogger<GroupController> _logger;

		public GroupController(IRepository<Group> groupRepository, ILogger<GroupController> logger)
		{
			this.groupRepository = groupRepository;
			this._logger = logger;
		}

		[HttpGet]
		[Route("GetAll")]
		public async Task<IEnumerable<Group>> GetAll()
		{
			return await groupRepository.GetAll();
		}

		[HttpGet]
		[Route("GetById")]
		public async Task<Group> GetById(int id)
		{
			return await groupRepository.GetById(id);
		}

		[HttpPost]
		[Route("Add")]
		[Authorize(Roles = "Administrator")]
		public async Task<ActionResult> Add(Group group)
		{
			return Ok(await groupRepository.Insert(group));
		}

		[HttpPost]
		[Route("Update")]
		[Authorize(Roles = "Administrator")]
		public async Task<ActionResult> Update(Group group)
		{
			return Ok(await groupRepository.Update(group));
		}

		[HttpPost]
		[Route("Delete")]
		[Authorize(Roles = "Administrator")]
		public async void Delete(Group group)
		{
			await groupRepository.Delete(group);
		}
	}
}