﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using University.Core.DAL.Models;
using University.Core.DAL.Repositories;
using University.ViewModel;

namespace University.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[Authorize(Roles = "Administrator")]
	public class WeekScheduleController : ControllerBase
	{
		private readonly IWeekScheduleRepository _weekScheduleRepository;
		private readonly ILogger<WeekScheduleController> _logger;
        private readonly IHostingEnvironment _appEnvironment;


        public WeekScheduleController(IWeekScheduleRepository weekScheduleRepository, IHostingEnvironment appEnvironment, ILogger<WeekScheduleController> logger)
		{
            _appEnvironment = appEnvironment;
			_weekScheduleRepository = weekScheduleRepository;
			_logger = logger;
		}

		[HttpGet]
		[Route("GetAll")]
		public async Task<IEnumerable<WeekScheduleView>> GetAll()
		{
			var schedules = await _weekScheduleRepository.Entities
				.Include(s => s.Subject)
				.Include(s => s.Teacher)
				.Include(s => s.ScheduleType)
				.Include(s => s.Group).ThenInclude(p => p.Specialty)
				.Include(s => s.Auditory).ThenInclude(a => a.Building).ToListAsync();

			return schedules.Select(s => Mapper.Map<WeekSchedule, WeekScheduleView>(s));
		}

		[HttpGet]
		[Route("GetById")]
		public async Task<IActionResult> GetById(int id)
		{
			var schedule = await _weekScheduleRepository.Entities
				.Include(s => s.Subject)
				.Include(s => s.Teacher)
				.Include(s => s.ScheduleType)
				.Include(s => s.Group).ThenInclude(p => p.Specialty)
				.Include(s => s.Auditory).ThenInclude(a => a.Building)
				.SingleOrDefaultAsync(s => s.Id == id);

			if (schedule == null) return NotFound();

			return Ok(schedule);
		}

		[HttpPost]
		[Route("Add")]
		public async Task<ActionResult> Add(WeekSchedule schedule)
		{
			return Ok(await _weekScheduleRepository.Insert(schedule));
		}

		[HttpPost]
		[Route("Update")]
		public async Task<ActionResult> Update(WeekSchedule schedule)
		{
			return Ok(await _weekScheduleRepository.Update(schedule));
		}

		[HttpPost]
		[Route("Delete")]
		public async void Delete(WeekSchedule schedule)
		{
			await _weekScheduleRepository.Delete(schedule);
		}


		[HttpPost]
		[Route("ImportSchedule")]
		public bool ImportSchedule(IFormFile file)
		{
			return _weekScheduleRepository.ImportSchedule(file);
		}

		[HttpGet]
		[Route("GenerateSchedule")]
		public bool GenerateSchedule(DateTime start, DateTime end)
		{
			return _weekScheduleRepository.GenerateSchedule(start, end);
		}
	}
}