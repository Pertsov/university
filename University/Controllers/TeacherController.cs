﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using University.Core.DAL;
using University.Core.DAL.Models;
using University.ViewModel;

namespace University.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[Authorize]
	public class TeacherController : ControllerBase
	{
		private readonly IRepository<Teacher> _teacherRepository;
		private readonly ILogger<TeacherController> _logger;

		public TeacherController(IRepository<Teacher> teacherRepository, ILogger<TeacherController> logger)
		{
			this._teacherRepository = teacherRepository;
			this._logger = logger;
		}

		[HttpGet]
		[Route("GetAll")]
		public async Task<IEnumerable<Teacher>> GetAll()
		{
			return await _teacherRepository.GetAll();			
		}

		[HttpGet]
		[Route("GetById")]
		public async Task<IActionResult> GetById(int id)
		{
			var teacher = await _teacherRepository.GetById(id);

			if (teacher == null) return NotFound();

			return Ok(teacher);
		}

		[HttpPost]
		[Route("Add")]
		[Authorize(Roles = "Administrator")]
		public async Task<ActionResult> Add(Teacher teacher)
		{
			return Ok(await _teacherRepository.Insert(teacher));
		}

		[HttpPost]
		[Route("Update")]
		[Authorize(Roles = "Administrator")]
		public async Task<ActionResult> Update(Teacher teacher)
		{
			return Ok(await _teacherRepository.Update(teacher));
		}

		[HttpPost]
		[Route("Delete")]
		[Authorize(Roles = "Administrator")]
		public async void Delete(Teacher teacher)
		{
			await _teacherRepository.Delete(teacher);
		}
	}
}