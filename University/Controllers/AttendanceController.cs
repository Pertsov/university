﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using University.Core.DAL;
using University.Core.DAL.Models;
using University.Core.DAL.Repositories;
using University.ViewModel;

namespace University.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
	[Authorize]
	public class AttendanceController : ControllerBase
    {
		private readonly IAttendanceRepository _attendanceRepository;
		private readonly ILogger<AttendanceController> _logger;

		public AttendanceController(IAttendanceRepository attendanceRepository, ILogger<AttendanceController> logger)
		{
			this._attendanceRepository = attendanceRepository;
			this._logger = logger;
		}

		[HttpGet]
		[Route("GetAll")]
		[Authorize(Roles = "Administrator")]
		public async Task<IEnumerable<Attendance>> GetAll()
		{
			return await _attendanceRepository.GetAll();
		}

		[HttpGet]
		[Route("Get")]
		public async Task<ActionResult> Get(int scheduleId)
		{
			var attendances = await _attendanceRepository.Get(scheduleId);

			if (attendances == null) return BadRequest();

			return Ok(attendances.Select(a => Mapper.Map<Attendance, AttendanceView>(a)));
		}


		[HttpGet]
		[Route("GetById")]
		public async Task<Attendance> GetById(int id)
		{
			return await _attendanceRepository.GetById(id);
		}

		[HttpPost]
		[Route("Add")]
		[Authorize(Roles = "Administrator")]
		public async Task<ActionResult> Add(Attendance attendance)
		{
			return Ok(await _attendanceRepository.Insert(attendance));
		}

		[HttpPost]
		[Route("Update")]
		[Authorize(Roles = "Administrator, Teacher")]
		public async Task<ActionResult> Update(IEnumerable<Attendance> attendances)
		{
			try
			{
				foreach (var attendance in attendances)
				{
					await _attendanceRepository.Update(attendance);
				};
			}
			catch (Exception e)
			{
				return BadRequest(e);
			}
			return Ok();
		}

		[HttpPost]
		[Route("Delete")]
		[Authorize(Roles = "Administrator")]
		public async void Delete(Attendance attendance)
		{
			await _attendanceRepository.Delete(attendance);
		}
	}
}