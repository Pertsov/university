﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using University.Core.DAL;
using University.Core.DAL.Models;

namespace University.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
	[Authorize]
	public class ScheduleTypeController : ControllerBase
    {
		private readonly IRepository<ScheduleType> scheduleTypeRepository;
		private readonly ILogger<ScheduleTypeController> _logger;

		public ScheduleTypeController(IRepository<ScheduleType> scheduleTypeRepository, ILogger<ScheduleTypeController> logger)
		{
			this.scheduleTypeRepository = scheduleTypeRepository;
			this._logger = logger;
		}

		[HttpGet]
		[Route("GetAll")]
		public async Task<IEnumerable<ScheduleType>> GetAll()
		{
			return await scheduleTypeRepository.GetAll();
		}

		[HttpGet]
		[Route("GetById")]
		public async Task<ScheduleType> GetById(int id)
		{
			return await scheduleTypeRepository.GetById(id);
		}

		[HttpPost]
		[Route("Add")]
		[Authorize(Roles = "Administrator")]
		public async Task<ActionResult> Add(ScheduleType scheduleType)
		{
			return Ok(await scheduleTypeRepository.Insert(scheduleType));
		}

		[HttpPost]
		[Route("Update")]
		[Authorize(Roles = "Administrator")]
		public async Task<ActionResult> Update(ScheduleType scheduleType)
		{
			return Ok(await scheduleTypeRepository.Update(scheduleType));
		}

		[HttpPost]
		[Route("Delete")]
		[Authorize(Roles = "Administrator")]
		public async void Delete(ScheduleType scheduleType)
		{
			await scheduleTypeRepository.Delete(scheduleType);
		}
	}
}