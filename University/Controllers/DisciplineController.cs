﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using University.Core.DAL;
using University.Core.DAL.Models;

namespace University.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
	[Authorize]
	public class DisciplineController : ControllerBase
    {
		private readonly IRepository<Discipline> disciplineRepository;
		private readonly ILogger<DisciplineController> _logger;
		 
		public DisciplineController(IRepository<Discipline> disciplineRepository, ILogger<DisciplineController> logger)
		{
			this.disciplineRepository = disciplineRepository;
			this._logger = logger;
		}

		[HttpGet]
		[Route("GetAll")]
		public async Task<IEnumerable<Discipline>> GetAll()
		{
			return await disciplineRepository.GetAll();
		}

		[HttpGet]
		[Route("GetById")]
		public async Task<Discipline> GetById(int id)
		{
			return await disciplineRepository.GetById(id);
		}

		[HttpPost]
		[Route("Add")]
		[Authorize(Roles = "Administrator")]
		public async Task<ActionResult> Add(Discipline discipline)
		{
			return Ok(await disciplineRepository.Insert(discipline));
		}

		[HttpPost]
		[Route("Update")]
		[Authorize(Roles = "Administrator")]
		public async Task<ActionResult> Update(Discipline discipline)
		{
			return Ok(await disciplineRepository.Update(discipline));
		}

		[HttpPost]
		[Route("Delete")]
		[Authorize(Roles = "Administrator")]
		public async void Delete(Discipline discipline)
		{
			await disciplineRepository.Delete(discipline);
		}
	}
}