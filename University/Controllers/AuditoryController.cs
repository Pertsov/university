﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using University.Core.DAL;
using University.Core.DAL.Models;
using University.ViewModel;

namespace University.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
	[Authorize]
	public class AuditoryController : ControllerBase
    {
		private readonly IRepository<Auditory> auditoryRepository;
		private readonly ILogger<AuditoryController> _logger;

		public AuditoryController(IRepository<Auditory> auditoryRepository, ILogger<AuditoryController> logger)
		{
			this.auditoryRepository = auditoryRepository;
			this._logger = logger;
		}

		[HttpGet]
		[Route("GetAll")]
		public async Task<IEnumerable<AuditoryView>> GetAll()
		{
			var auditories = await auditoryRepository.Entities
				.Include(s => s.AuditoryType)
				.Include(s => s.Building).ToListAsync();

			return auditories.Select(s => Mapper.Map<Auditory, AuditoryView>(s));
		}

		[HttpGet]
		[Route("GetById")]
		public async Task<IActionResult> GetById(int id)
		{
			var auditory = await auditoryRepository.Entities
				.Include(a => a.AuditoryType)
				.Include(a => a.Building)
				.SingleOrDefaultAsync(a => a.Id == id);

			if (auditory == null) return NotFound();

			return Ok(auditory);
		}

		[HttpPost]
		[Route("Add")]
		[Authorize(Roles = "Administrator")]
		public async Task<ActionResult> Add(Auditory auditory)
		{
			return Ok(await auditoryRepository.Insert(auditory));
		}
      
        [HttpPost]
		[Route("Update")]
		[Authorize(Roles = "Administrator")]
		public async Task<ActionResult> Update(Auditory auditory)
		{
			return Ok(await auditoryRepository.Update(auditory));
		}

		[HttpPost]
		[Route("Delete")]
		[Authorize(Roles = "Administrator")]
		public async void Delete(Auditory auditory)
		{
			await auditoryRepository.Delete(auditory);
		}
	}
}