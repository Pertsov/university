﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using University.Core.DAL;
using Newtonsoft.Json.Serialization;
using AutoMapper;
using University.ViewModel;
using University.Core.DAL.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using University.Core.DAL.Helpers;
using System.IdentityModel.Tokens.Jwt;
using System.Reflection;
using Swashbuckle.AspNetCore.Swagger;

namespace University
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
			services.AddDbContext<DataContext>(options => options.UseSqlServer(Configuration.GetConnectionString("UniversityDB"), x => x.MigrationsAssembly("University.Core")));

			// ===== Add Identity ========
			services.AddIdentity<IdentityUser, IdentityRole>(options =>
			{
				options.Password.RequireDigit = false;
				options.Password.RequiredLength = 25;
				options.Password.RequireNonAlphanumeric = false;
				options.Password.RequireUppercase = false;
				options.Password.RequireLowercase = false;
			})
				.AddEntityFrameworkStores<DataContext>()
				.AddDefaultTokenProviders();

			//JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
			services.AddAuthentication(options =>
			{
				options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
				options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
			})
			.AddJwtBearer(options =>
			{
				options.RequireHttpsMetadata = false;
				options.SaveToken = true;
				options.TokenValidationParameters = new TokenValidationParameters
				{
					ValidateIssuer = true,
					ValidIssuer = AuthOptions.ISSUER,
					ValidateAudience = false,
					ValidAudience = AuthOptions.AUDIENCE,
					ValidateLifetime = true,
					IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),
					ValidateIssuerSigningKey = true,
				};
			});
			
			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1).AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());
			services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
			services.AddScoped(typeof(IWeekScheduleRepository), typeof(WeekScheduleRepository));
			services.AddScoped(typeof(IAttendanceRepository), typeof(AttendanceRepository));
			services.AddScoped(typeof(IUserRepository), typeof(UserRepository));

			// Register the Swagger services
			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1.0", new Info { Title = "University API v1.0", Version = "v1.0" });

				// Swagger 2.+ support
				var security = new Dictionary<string, IEnumerable<string>>
				{
					{"Bearer", new string[] { }},
				};

				c.AddSecurityDefinition("Bearer", new ApiKeyScheme
				{
					Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
					Name = "Authorization",
					In = "header",
					Type = "apiKey"
				});
				c.AddSecurityRequirement(security);
			});

			// Enable Cross-Origin Requests (CORS) in ASP.NET Core
			services.AddCors();    

			Mapper.Initialize(cfg =>
			{
				cfg.AddProfile(typeof(ScheduleProfile));
				cfg.AddProfile(typeof(WeekScheduleProfile));
				cfg.AddProfile(typeof(AuditoryProfile));
				cfg.AddProfile(typeof(ScheduleDetailsProfile));
			});
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env, DataContext dbContext)
        {

			app.UseSwagger();
			app.UseSwaggerUI(c =>
			{
				c.SwaggerEndpoint("/swagger/v1.0/swagger.json", "Versioned API v1.0");
			});


			app.UseAuthentication();
			if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

			app.UseCors(builder => builder
				.WithOrigins("http://localhost:8080")
				.WithOrigins("https://nmalish.github.io")
				.AllowAnyHeader()
				.AllowAnyMethod()
			);

			app.UseHttpsRedirection();
			app.UseDefaultFiles();
			app.UseStaticFiles();

			app.UseMvc();
			
		}
    }
}
