﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Web;
using University.Core.DAL.Services;

namespace University
{
    public class Program
	{
		public static void Main(string[] args)
        {
			// NLog: setup the logger first to catch all errors
			var logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
			var host = BuildWebHost(args);

			try
			{
				logger.Debug("init main");
				using (var scope = host.Services.CreateScope())
				{
					var services = scope.ServiceProvider;
					var userManager = services.GetRequiredService<UserManager<IdentityUser>>();
					var rolesManager = services.GetRequiredService<RoleManager<IdentityRole>>();

					RoleInitializer.InitializeAsync(userManager, rolesManager).GetAwaiter().GetResult();
				}
				host.Run();

				//CreateWebHostBuilder(args).Build().Run();
			}
			catch (Exception ex)
			{
				//NLog: catch setup errors
				logger.Error(ex, "Stopped program because of exception");

			}
			finally
			{
				// Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
				NLog.LogManager.Shutdown();
			}
        }

        public static IWebHost BuildWebHost(string[] args) =>
                WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
			WebHost.CreateDefaultBuilder(args)
				.UseStartup<Startup>()
				.ConfigureLogging(logging =>
				{
					logging.ClearProviders();
					logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
				})
				.UseNLog();      // NLog: setup NLog for Dependency injection
	}
}
