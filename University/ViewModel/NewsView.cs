﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using University.Core.DAL.Models;

namespace University.ViewModel
{
	public class NewsView
	{
		public string Author { get; set; }
		public string Title { get; set; }
		public string Text { get; set; }
		public Guid Image { get; set; }
		public IFormFile File { get; set; }
		public int FacultyId { get; set; }
		public string ApplicationUserId { get; set; }
		[JsonIgnore]
		public Faculty Faculty { get; set; }
	}
}
