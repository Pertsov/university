﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace University.ViewModel
{
	public class UserView
	{
		public string Id { get; set; }
		public string UserName { get; set; }
		public string Email { get; set; }
		public string PhoneNumber { get; set; }
	}

	public class UserProfile : Profile
	{
		public UserProfile()
		{
			CreateMap<IdentityUser, WeekScheduleView>();
		}
	}

	public class CreateUserViewModel
	{
		public string Email { get; set; }
		public string Password { get; set; }
	}
	public class EditUserViewModel
	{
		public string Id { get; set; }
		public string Email { get; set; }
	}

	public class ChangePasswordViewModel
	{
		public string Id { get; set; }
		public string Email { get; set; }
		public string NewPassword { get; set; }
		public string OldPassword { get; set; }
	}

	public class ChangeRoleViewModel
	{
		public string UserId { get; set; }
		public string UserEmail { get; set; }
		public List<IdentityRole> AllRoles { get; set; }
		public IList<string> UserRoles { get; set; }
		public ChangeRoleViewModel()
		{
			AllRoles = new List<IdentityRole>();
			UserRoles = new List<string>();
		}
	}

	
	public class LoginDto
	{
		[Required]
		public string Email { get; set; }
		[Required]
		public string Password { get; set; }

	}
}
