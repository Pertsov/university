﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using University.Core.DAL.Models;

namespace University.ViewModel
{
	public class AuditoryView
	{
		public int Id { get; set; }
		public int Amount { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public string AuditoryType { get; set; }
		public string BuildingName { get; set; }
	}

	public class AuditoryProfile : Profile
	{
		public AuditoryProfile()
		{
			CreateMap<Auditory, AuditoryView>()
				.ForMember(dest => dest.AuditoryType, opts => opts.MapFrom(src => src.AuditoryType.Name))
				.ForMember(dest => dest.BuildingName, opts => opts.MapFrom(src => src.Building.Name));
		}
	}
}
