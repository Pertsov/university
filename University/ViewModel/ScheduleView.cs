﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using University.Core.DAL.Models;

namespace University.ViewModel
{
	public class ScheduleView
	{
		public int Id { get; set; }
		public int LessonNumber { get; set; }
		public DateTime Date { get; set; }
		public TimeSpan Start { get; set; }
		public TimeSpan End { get; set; }
		public string ScheduleTypeName { get; set; }
		public string BuildingName { get; set; }
		public string AuditoryName { get; set; }
		public string SubjectName { get; set; }
		public string TeacherName { get; set; }
		public string GroupName { get; set; }
		public int AuditoryId { get; set; }
		public int SubjectId { get; set; }
		public int TeacherId { get; set; }
		public int GroupId { get; set; }
	}

	public class ScheduleProfile : Profile
	{
		public ScheduleProfile()
		{
			CreateMap<Schedule, ScheduleView>()
				.ForMember(dest => dest.AuditoryName, opts => opts.MapFrom(src => src.Auditory.Name))
				.ForMember(dest => dest.TeacherName, opts => opts.MapFrom(src => src.Teacher.Description))
				.ForMember(dest => dest.SubjectName, opts => opts.MapFrom(src => src.Subject.Name))
				.ForMember(dest => dest.GroupName, opts => opts.MapFrom(src => src.Group.Name))
				.ForMember(dest => dest.BuildingName, opts => opts.MapFrom(src => src.Auditory.Building.Name))
				.ForMember(dest => dest.ScheduleTypeName, opts => opts.MapFrom(src => src.ScheduleType.Name));
		}
	}
	public class ScheduleDetailsView
	{
		public int Id { get; set; }
		public int LessonNumber { get; set; }
		public DateTime Date { get; set; }
		public TimeSpan Start { get; set; }
		public TimeSpan End { get; set; }
		public int ScheduleTypeId { get; set; }
		public int AuditoryId { get; set; }
		public int SubjectId { get; set; }
		public int TeacherId { get; set; }
		public int GroupId { get; set; }


		public Auditory Auditory { get; set; }
		public ScheduleType ScheduleType { get; set; }
		public Subject Subject { get; set; }
		public Teacher Teacher { get; set; }
		public Group Group { get; set; }
		public Note Note { get; set; }
		public ICollection<Note> Messages { get; set; }
	}

	public class ScheduleFilter 
	{
		public DateTime Start { get; set; }
		public DateTime? End { get; set; }
		public string UserId { get; set; }
		public int? AuditoryId { get; set; }
		public int? GroupId{ get; set; }
		public int? TeacherId { get; set; }
		public int? SubjectId { get; set; }
	}

	public class ScheduleDetailsProfile : Profile
	{
		public ScheduleDetailsProfile()
		{
			CreateMap<Schedule, ScheduleDetailsView>()
				.ForMember(dest => dest.Note, opts => opts.MapFrom(src => src.Notes.FirstOrDefault(u => u.ApplicationUserId == src.Group.Students.First().ApplicationUserId)))
				.ForMember(dest => dest.Messages, opts => opts.MapFrom(src => src.Notes.Where(u => u.ApplicationUserId == src.Teacher.ApplicationUserId)));
				//.ForMember(dest => dest.Attendances, opts => opts.MapFrom(src => src.Attendances.Where(u => u.Student.Group.Id == src.GroupId)));
		}
	}
}
