﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using University.Core.DAL.Models;

namespace University.ViewModel
{
	public class SpecialtyView
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public string FacultyName { get; set; }
	}

	public class SpecialtyProfile : Profile
	{
		public SpecialtyProfile()
		{
			CreateMap<Specialty, SpecialtyView>()
				.ForMember(dest => dest.FacultyName, opts => opts.MapFrom(src => src.Faculty.Name));
		}
	}
}
