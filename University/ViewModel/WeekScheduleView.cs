﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using University.Core.DAL.Models;

namespace University.ViewModel
{
	public class WeekScheduleView
	{
		public int Id { get; set; }
		public int Day { get; set; }
		public int LessonNumber { get; set; }
		public string ScheduleTypeName { get; set; }
		public string BuildingName { get; set; }
		public string AuditoryName { get; set; }
		public string SubjectName { get; set; }
		public string TeacherName { get; set; }
		public string GroupName { get; set; }
		public string SpecialtyName { get; set; }
	}

	public class WeekScheduleProfile : Profile
	{
		public WeekScheduleProfile()
		{
			CreateMap<WeekSchedule, WeekScheduleView>()
				.ForMember(dest => dest.AuditoryName, opts => opts.MapFrom(src => src.Auditory.Name))
				.ForMember(dest => dest.GroupName, opts => opts.MapFrom(src => src.Group.Name))
				.ForMember(dest => dest.TeacherName, opts => opts.MapFrom(src => src.Teacher.Name))
				.ForMember(dest => dest.SubjectName, opts => opts.MapFrom(src => src.Subject.Name))
				.ForMember(dest => dest.SpecialtyName, opts => opts.MapFrom(src => src.Group.Specialty.Name))
				.ForMember(dest => dest.BuildingName, opts => opts.MapFrom(src => src.Auditory.Building.Name))
				.ForMember(dest => dest.ScheduleTypeName, opts => opts.MapFrom(src => src.ScheduleType.Name));
		}
	}
}
