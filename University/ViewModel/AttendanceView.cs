﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using University.Core.DAL.Models;

namespace University.ViewModel
{
	public class AttendanceView
	{
		public int Id { get; set; }
		public bool? CheckIn { get; set; }
		public DateTime Date { get; set; }
		public int ScheduleId { get; set; }
		public int StudentId { get; set; }
		public string StudentName { get; set; }
	}

	public class AttendanceProfile : Profile
	{
		public AttendanceProfile()
		{
			CreateMap<Attendance, AttendanceView>()
				.ForMember(dest => dest.StudentName, opts => opts.MapFrom(src => src.Student.Name));
		}
	}
}
