﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace University.Core.DAL.Services
{
	public class RoleInitializer
	{
		public static async Task InitializeAsync(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
		{
			string adminEmail = "admin@university.com";
			string password = "cc5a22e02a065cf9a9ea03796da47573";
			string teacherEmail = "teacher@university.com";
			string teacherPassword = "363b43d6e059280b4ac3d82e8f0225fd";
			string studentEmail = "student@university.com";
			string studentPassword = "e9fed0a2aee435f9e81dc48e330c38f4";
			string vitaliy202 = "vitaliy202@university.com";
			string vasyl202 = "vasyl202@university.com";

			if (await roleManager.FindByNameAsync("Administrator") == null)
			{
				await roleManager.CreateAsync(new IdentityRole("Administrator"));
			}
			if (await roleManager.FindByNameAsync("Teacher") == null)
			{
				await roleManager.CreateAsync(new IdentityRole("Teacher"));
			}
			if (await roleManager.FindByNameAsync("Student") == null)
			{
				await roleManager.CreateAsync(new IdentityRole("Student"));
			}
			if (await userManager.FindByNameAsync(adminEmail) == null)
			{
				IdentityUser admin = new IdentityUser { Email = adminEmail, UserName = adminEmail };
				IdentityResult result = await userManager.CreateAsync(admin, password);
				if (result.Succeeded)
				{
					await userManager.AddToRoleAsync(admin, "Administrator");
				}
			}
			if (await userManager.FindByNameAsync(teacherEmail) == null)
			{
				IdentityUser teacher = new IdentityUser { Email = teacherEmail, UserName = teacherEmail };
				IdentityResult result = await userManager.CreateAsync(teacher, teacherPassword);
				if (result.Succeeded)
				{
					await userManager.AddToRoleAsync(teacher, "Teacher");
				}
			}
			if (await userManager.FindByNameAsync(studentEmail) == null)
			{
				IdentityUser student = new IdentityUser { Email = studentEmail, UserName = studentEmail };
				IdentityResult result = await userManager.CreateAsync(student, studentPassword);
				if (result.Succeeded)
				{
					await userManager.AddToRoleAsync(student, "Student");
				}
			}
			if (await userManager.FindByNameAsync(vitaliy202) == null)
			{
				IdentityUser student = new IdentityUser { Email = vitaliy202, UserName = vitaliy202 };
				IdentityResult result = await userManager.CreateAsync(student, studentPassword);
				if (result.Succeeded)
				{
					await userManager.AddToRoleAsync(student, "Student");
				}
			}
			if (await userManager.FindByNameAsync(vasyl202) == null)
			{
				IdentityUser student = new IdentityUser { Email = vasyl202, UserName = vasyl202 };
				IdentityResult result = await userManager.CreateAsync(student, studentPassword);
				if (result.Succeeded)
				{
					await userManager.AddToRoleAsync(student, "Student");
				}
			}
		}
	}
}
