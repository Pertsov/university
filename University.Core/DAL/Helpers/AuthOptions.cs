﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Text;

namespace University.Core.DAL.Helpers
{
	public class AuthOptions
	{
		public const string ISSUER = "UniversityAPIAuthServer";
		public const string AUDIENCE = "https://universityapi.azurewebsites.net";
		public const string KEY = "9E92C2664F1B2515A98BBB9F3492B";
		public const int LIFETIME = 1;
		public static SymmetricSecurityKey GetSymmetricSecurityKey()
		{
			return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
		}
	}
}
