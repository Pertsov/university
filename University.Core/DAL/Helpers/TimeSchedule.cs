﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace University.Core.DAL.Helpers
{
	public class TimeSchedule
	{
		public static readonly Dictionary<int, ST> Schedule = new Dictionary<int, ST>()
		{	
			{ 1, new ST { Start = TimeSpan.Parse("08:20"), End = TimeSpan.Parse("09:40") } },
			{ 2, new ST { Start = TimeSpan.Parse("09:50"), End = TimeSpan.Parse("11:10") } },
			{ 3, new ST { Start = TimeSpan.Parse("11:30"), End = TimeSpan.Parse("12:50") } },
			{ 4, new ST { Start = TimeSpan.Parse("13:00"), End = TimeSpan.Parse("14:20") } },
			{ 5, new ST { Start = TimeSpan.Parse("14:40"), End = TimeSpan.Parse("16:00") } },
			{ 6, new ST { Start = TimeSpan.Parse("16:10"), End = TimeSpan.Parse("17:30") } }
		};
	}

	public class ST
	{
		public TimeSpan Start { get; set; }
		public TimeSpan End { get; set; }
	}
}
