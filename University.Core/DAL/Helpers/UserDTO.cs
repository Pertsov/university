﻿using System;
using System.Collections.Generic;
using System.Text;

namespace University.Core.DAL.Helpers
{
	public class TokenResult
	{
		public string Access_token { get; set; }
		public DateTime Expires { get; set; }

	}
}
