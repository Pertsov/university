﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using University.Core.DAL.Helpers;
using University.Core.DAL.Models;

namespace University.Core.DAL.Repositories
{
	public class UserRepository : IUserRepository
	{
		private readonly DataContext _context;
		private readonly UserManager<IdentityUser> _userManager;
		public UserRepository (DataContext context, UserManager<IdentityUser> userManager)
		{
			_context = context;
			_userManager = userManager;
		}
		public async Task<object> GenerateJwtToken(string email, IdentityUser user)
		{
			var claims = new List<Claim>
			{
				new Claim(JwtRegisteredClaimNames.Sub, email),
				new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
				new Claim(ClaimTypes.Name, user.Id),
			};
			var roles = await _userManager.GetRolesAsync(user);
			if (roles != null)
			{
				foreach (var role in roles)
				{
					claims.Add(new Claim(ClaimTypes.Role, role));
				}
			}

			var key = AuthOptions.GetSymmetricSecurityKey();
			var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
			var expires = DateTime.Now.AddDays(Convert.ToDouble(AuthOptions.LIFETIME));

			var token = new JwtSecurityToken(
				AuthOptions.ISSUER,
				AuthOptions.ISSUER,
				claims,
				expires: expires,
				signingCredentials: creds
			);
			var result = new TokenResult
			{
				Expires = expires,
				Access_token = new JwtSecurityTokenHandler().WriteToken(token)
			};
			return result;
		}
		 
		public async Task<Teacher> GetTeacherInfo (string userId)
		{
			return await _context.Teachers.FirstOrDefaultAsync(t => t.ApplicationUserId == userId);
		}

		public async Task<Student> GetStudentInfo (string userId)
		{
			return await _context.Students.Include(s => s.Group).FirstOrDefaultAsync(s => s.ApplicationUserId == userId);
		}
	}
}
