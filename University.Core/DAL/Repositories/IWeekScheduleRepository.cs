﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using University.Core.DAL.Models;

namespace University.Core.DAL.Repositories
{
	public interface IWeekScheduleRepository : IRepository<WeekSchedule> 
	{
		bool ImportSchedule(IFormFile file);

		bool GenerateSchedule(DateTime start, DateTime end);
		void WriteToDataBase();
	}
}
