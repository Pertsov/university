﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using ScheduleParser;
using ScheduleParser.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using University.Core.DAL.Helpers;
using University.Core.DAL.Models;

namespace University.Core.DAL.Repositories
{
	public class AttendanceRepository : IAttendanceRepository
	{
		private readonly DataContext context;
		private DbSet<Attendance> entities;

		public AttendanceRepository(DataContext context)
		{
			this.context = context;
            entities = context.Set<Attendance>();
		}

		public IQueryable<Attendance> Entities => entities.AsQueryable();

		public async Task Delete(Attendance entity)
		{
			if (entity == null)
			{
				throw new ArgumentNullException();
			}

			entities.Remove(entity);
			await context.SaveChangesAsync();
		}

		public async Task<IEnumerable<Attendance>> Get(Expression<Func<Attendance, bool>> predicate)
		{
			return await entities.Where(predicate).ToListAsync();
		}

		public async Task<IEnumerable<Attendance>> Get(int scheduleId)
		{
			var schedule = context.Schedules.SingleOrDefault(s => s.Id == scheduleId && s.Date == DateTime.Now.Date);

			if (schedule == null) return null;

			var attendances = await context.Attendances.Include(s => s.Schedule).Include(s => s.Student).Where(s => s.ScheduleId == scheduleId).ToListAsync();

			if (attendances.Count != 0) return attendances;

			var students = context.Students.Where(s => s.GroupId == schedule.GroupId);
			
			foreach (var student in students)
			{
				var item = new Attendance
				{
					Date = DateTime.Now.Date,
					CheckIn = false,
					ScheduleId = schedule.Id,
					CreatedBy = "teacher",
					CreatedOn = DateTime.Now,
					ModifiedOn = DateTime.Now,
					StudentId = student.Id
				};
				context.Attendances.Add(item);
			}
			context.SaveChanges();

			return await context.Attendances.Include(s => s.Student)
				.Include(s => s.Schedule).Include(s => s.Student).Where(s => s.ScheduleId == scheduleId).ToListAsync();
		}

		public async Task<IEnumerable<Attendance>> GetAll()
		{
			return await entities.ToListAsync();
		}

		public async Task<Attendance> GetById(int id)
		{
			return await entities.SingleOrDefaultAsync(e => e.Id == id);
		}

		public async Task<Attendance> Insert(Attendance entity)
		{
			if (entity == null)
			{
				throw new ArgumentNullException();
			}

			entity.CreatedBy = "1";
			entity.ModifiedOn = DateTime.Now;
			entity.CreatedOn = DateTime.Now;

			entities.Add(entity);
			await context.SaveChangesAsync();
			return entity;
		}

		public async Task<Attendance> Update(Attendance entity)
		{
			if (entity == null)
			{
				throw new ArgumentNullException();
			}

			entity.ModifiedBy = "2";
			entity.ModifiedOn = DateTime.Now;

			context.Entry(entity).Property(x => x.CreatedBy).IsModified = false;
			context.Entry(entity).Property(x => x.CreatedOn).IsModified = false;

			context.Entry(entity).State = EntityState.Modified;

			await context.SaveChangesAsync();
			return entity;
		}

    }

}
