﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using University.Core.DAL.Models;

namespace University.Core.DAL.Repositories
{
	public interface IUserRepository 
	{
		Task<Student> GetStudentInfo (string userId);
		Task<Teacher> GetTeacherInfo (string userId);
		Task<object> GenerateJwtToken(string email, IdentityUser user);
	}
}
