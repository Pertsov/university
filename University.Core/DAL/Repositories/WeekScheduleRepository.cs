﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using ScheduleParser;
using ScheduleParser.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using University.Core.DAL.Helpers;
using University.Core.DAL.Models;

namespace University.Core.DAL.Repositories
{
	public class WeekScheduleRepository : IWeekScheduleRepository
	{
		IHostingEnvironment _appEnvironment;
		private readonly DataContext context;
		private DbSet<WeekSchedule> entities;

		public WeekScheduleRepository(DataContext context , IHostingEnvironment hostingEnvironment)
		{
			_appEnvironment = hostingEnvironment;
			this.context = context;
            _appEnvironment = hostingEnvironment;
            entities = context.Set<WeekSchedule>();
		}

		public IQueryable<WeekSchedule> Entities => entities.AsQueryable();

		public async Task Delete(WeekSchedule entity)
		{
			if (entity == null)
			{
				throw new ArgumentNullException();
			}

			entities.Remove(entity);
			await context.SaveChangesAsync();
		}

		public async Task<IEnumerable<WeekSchedule>> Get(Expression<Func<WeekSchedule, bool>> predicate)
		{
			return await entities.Where(predicate).ToListAsync();
		}

		public async Task<IEnumerable<WeekSchedule>> GetAll()
		{
			return await entities.ToListAsync();
		}

		public async Task<WeekSchedule> GetById(int id)
		{
			return await entities.SingleOrDefaultAsync(e => e.Id == id);
		}

		public async Task<WeekSchedule> Insert(WeekSchedule entity)
		{
			if (entity == null)
			{
				throw new ArgumentNullException();
			}

			entity.CreatedBy = "1";
			entity.ModifiedOn = DateTime.Now;
			entity.CreatedOn = DateTime.Now;

			entities.Add(entity);
			await context.SaveChangesAsync();
			return entity;
		}

		public async Task<WeekSchedule> Update(WeekSchedule entity)
		{
			if (entity == null)
			{
				throw new ArgumentNullException();
			}

			entity.ModifiedBy = "2";
			entity.ModifiedOn = DateTime.Now;

			context.Entry(entity).Property(x => x.CreatedBy).IsModified = false;
			context.Entry(entity).Property(x => x.CreatedOn).IsModified = false;

			context.Entry(entity).State = EntityState.Modified;

			await context.SaveChangesAsync();
			return entity;
		}

		public bool ImportSchedule (IFormFile file)
		{
			// ':' - Invalid character in file name 
			// FileName format schedule_yyyy-MM-dd_HH-mm-ss.xls
			// Temp solution, in future will create table in database and use Guide for filename

			string path = _appEnvironment.WebRootPath + "\\Files\\";
			bool exists = System.IO.Directory.Exists(path);

			if (!exists)
				System.IO.Directory.CreateDirectory(path);

			long size = file.Length;
			string fileName = "schedule_" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + Path.GetExtension(file.FileName);

			if (file.Length > 0)
			{
				try
				{
					using (var stream = new FileStream(path + fileName, FileMode.Create))
					{
						file.CopyTo(stream);
					}

                    ScheduleParseProcess.Initialization(path + fileName);
					WriteToDataBase();

					return true;
				}
				catch (Exception e)
				{
					return false;
				}
			}
			return false;
		}

		public bool GenerateSchedule(DateTime start, DateTime end)
		{
			if (start < DateTime.Today) start = DateTime.Today;
			for (DateTime i = start; i <= end; i = i.AddDays(1))
			{
				var daySchedule = context.WeekSchedules.Where(w => w.Day == (int)i.DayOfWeek).ToList();
				foreach (var d in daySchedule)
				{
					var item = new Schedule
					{
						AuditoryId = d.AuditoryId,
						ScheduleTypeId = d.ScheduleTypeId,
						SubjectId = d.SubjectId,
						Date = i,
						LessonNumber = d.LessonNumber,
						TeacherId = d.TeacherId,
						GroupId = d.GroupId,
						Start = TimeSchedule.Schedule.FirstOrDefault(k => k.Key == d.LessonNumber).Value.Start,
						End = TimeSchedule.Schedule.FirstOrDefault(k => k.Key == d.LessonNumber).Value.End,
						CreatedOn = DateTime.Today,
						CreatedBy = "generate",
						ModifiedOn = DateTime.Today,
					};
					context.Schedules.Add(item);
				}
			}
			context.SaveChanges();
			return true;
		}

		public void WriteToDataBase()
        {
            foreach (var s in ScheduleFile.Specialities)
            {
                if (context.Specialties.Where(e => e.Name == s.Speciality).Count() > 0) continue;
                var item = new Specialty
                {
                    Name = s.Speciality,
                    FacultyId = context.Faculties.FirstOrDefault(f => f.Name == "Факультет математики та інформатики").Id,
					CreatedBy = "parser",
                    Description = " ",
                    CreatedOn = DateTime.Today,
                    ModifiedOn = DateTime.Today
                };
                context.Specialties.Add(item);
            }
            context.SaveChanges();
            foreach (var g in ScheduleFile.Groups)
            {
                if (context.Groups.Where(e => e.Name == g.GroupName && e.Specialty.Name == g.SpecialityName).Count() > 0) continue;
                var item = new Group
                {
                    Semester = 1,
                    CreatedBy = "parser",
                    Name = g.GroupName,
                    SpecialtyId = context.Specialties.FirstOrDefault(s => s.Name == g.SpecialityName).Id,
                    Year = 2018,
                    Description = " ",
                    CreatedOn = DateTime.Today,
                    ModifiedOn = DateTime.Today
                };
                context.Groups.Add(item);
            }
            context.SaveChanges();
            foreach (var name in ScheduleFile.Teachers)
            {
                if (context.Teachers.Where(t => t.Name == name).Count() > 0) continue;
                var item = new Teacher
                {
                    CreatedBy = "parser",
                    CreatedOn = DateTime.Today,
                    ModifiedOn = DateTime.Today,
                    Name = name,
                    Description = name,
                    Email = "mail"
                };
                context.Teachers.Add(item);
            }
            context.SaveChanges();
            foreach (var name in ScheduleFile.Auditories)
            {
                if (context.Auditories.Where(t => t.Name == name).Count() > 0) continue;
                var item = new Auditory
                {
                    CreatedBy = "parser",
                    CreatedOn = DateTime.Today,
                    ModifiedOn = DateTime.Today,
                    Name = name,
                    Description = name,
                    BuildingId = context.Buildings.FirstOrDefault(c => c.Name == "1").Id,
                    Amount = 30,
                    TypeId = context.AuditoryTypes.FirstOrDefault(c => c.Name == "CLASS").Id
				};
                context.Auditories.Add(item);
            }
            context.SaveChanges();
            foreach (var name in ScheduleFile.Subjects)
            {
                if (context.Subjects.Where(t => t.Name == name).Count() > 0) continue;
                var item = new Subject
                {
                    CreatedBy = "parser",
                    CreatedOn = DateTime.Today,
                    ModifiedOn = DateTime.Today,
                    Name = name,
                    Description = name,
                };
                context.Subjects.Add(item);
            }
            context.SaveChanges();
            foreach (var lesson in ScheduleFile.Lessons)
            {

                var group = context.Groups.FirstOrDefault(g => g.Name == lesson.GroupName);
                foreach (var e in lesson.Lessons)
                {
                    var a = context.Auditories.FirstOrDefault(c => c.Name == " ");   // will create entities in database where [Entity].[Name] == ' '
                    var s = context.Subjects.FirstOrDefault(c => c.Name == " ");
                    var st = context.ScheduleTypes.FirstOrDefault(c => c.Name == "LECTURE");
					var t = context.Teachers.FirstOrDefault(c => c.Name == " ");

                    if (e.Auditory != null) a = context.Auditories.FirstOrDefault(c => c.Name == e.Auditory);
                    if (e.Lesson != null) s = context.Subjects.FirstOrDefault(c => c.Name == e.Lesson);
                    if (e.Teacher != null) t = context.Teachers.FirstOrDefault(c => c.Description == e.Teacher);
					if (e.Type != null) st = context.ScheduleTypes.FirstOrDefault(c => c.Name == e.Type);

					if (context.WeekSchedules.Where(w => w.SubjectId == s.Id && w.TeacherId == t.Id && w.AuditoryId == a.Id && w.GroupId == group.Id).Count() > 0) continue;
                    Enum.TryParse(typeof(MyDayOfWeek), e.Day.Replace("'", ""), true, out object day);
                    var item = new WeekSchedule
                    {
                        CreatedBy = "parser",
                        CreatedOn = DateTime.Today,
                        ModifiedOn = DateTime.Today,
                        LessonNumber = int.Parse(e.LessonNumber),
                        Day = (int)day,
                        ScheduleTypeId = st.Id,
                        AuditoryId = a.Id,
                        SubjectId = s.Id,
                        TeacherId = t.Id,
                        GroupId = group.Id,
                    };
                    context.WeekSchedules.Add(item);

                }
                context.SaveChanges();
            }
        }

    }

}
