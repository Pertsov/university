﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using University.Core.Entities;

namespace University.Core.DAL
{
	public class Repository<T> : IRepository<T> where T : BaseEntity
	{
		private readonly DataContext _context;
		private readonly IHttpContextAccessor _httpContextAccessor;
		private DbSet<T> entities;

		public Repository(DataContext context, IHttpContextAccessor httpContextAccessor)
		{
			_context = context;
			_httpContextAccessor = httpContextAccessor;
			entities = context.Set<T>();
		}

		public IQueryable<T> Entities => entities.AsQueryable();

		public async virtual Task Delete(T entity)
		{
			if (entity == null)
			{
				throw new ArgumentNullException();
			}

			entities.Remove(entity);
			await _context.SaveChangesAsync();
		}

		public async virtual Task<IEnumerable<T>> Get(Expression<Func<T, bool>> predicate)
		{
			return await entities.Where(predicate).ToListAsync();
		}

		public async virtual Task<IEnumerable<T>> GetAll()
		{
			return await entities.ToListAsync();
		}

		public async virtual Task<T> GetById(int id)
		{
			return await entities.SingleOrDefaultAsync(e => e.Id == id);
		}

		public async virtual Task<T> Insert(T entity)
		{
			if (entity == null)
			{
				throw new ArgumentNullException();
			}

			entity.CreatedBy = _httpContextAccessor.HttpContext.User.Identity.Name;
			entity.ModifiedOn = DateTime.Now;
			entity.CreatedOn = DateTime.Now;

			entities.Add(entity);
			await _context.SaveChangesAsync();
			return entity;
		}

		public async virtual Task<T> Update(T entity)
		{
			if (entity == null)
			{
				throw new ArgumentNullException();
			}

			entity.ModifiedBy = _httpContextAccessor.HttpContext.User.Identity.Name;
			entity.ModifiedOn = DateTime.Now;

			_context.Entry(entity).Property(x => x.CreatedBy).IsModified = false;
			_context.Entry(entity).Property(x => x.CreatedOn).IsModified = false;

			_context.Entry(entity).State = EntityState.Modified;
			 
			await _context.SaveChangesAsync();
			return entity;
		}
	}
}
