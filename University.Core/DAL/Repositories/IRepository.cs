﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using University.Core.Entities;

namespace University.Core.DAL
{
	public interface IRepository<T> where T : BaseEntity
	{
		IQueryable<T> Entities { get; }

		Task<IEnumerable<T>> GetAll();

		Task<IEnumerable<T>> Get(Expression<Func<T, bool>> predicate);

		Task<T> GetById(int id);

		Task<T> Insert(T entity);

		Task<T> Update(T entity);

		Task Delete(T entity);
	}
}
