﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using University.Core.DAL.Models;

namespace University.Core.DAL.Repositories
{
	public interface IAttendanceRepository : IRepository<Attendance>
	{
		Task<IEnumerable<Attendance>> Get(int scheduleId);

	}
}
