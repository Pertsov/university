﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using University.Core.DAL.Models;

namespace University.Core.DAL
{
    public class DataContext : IdentityDbContext
	{
		public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        { }
		protected override void OnModelCreating(ModelBuilder builder)
		{
			base.OnModelCreating(builder);
		}

		public virtual DbSet<Attendance> Attendances { get; set; }
		public virtual DbSet<Auditory> Auditories { get; set; }
		public virtual DbSet<AuditoryType> AuditoryTypes { get; set; }
		public virtual DbSet<Building> Buildings { get; set; }
		public virtual DbSet<Discipline> Disciplines { get; set; }
		public virtual DbSet<Faculty> Faculties { get; set; }
		public virtual DbSet<Group> Groups { get; set; }
		public virtual DbSet<Schedule> Schedules { get; set; }
		public virtual DbSet<WeekSchedule> WeekSchedules { get; set; }
		public virtual DbSet<ScheduleType> ScheduleTypes { get; set; }
		public virtual DbSet<Specialty> Specialties { get; set; }
		public virtual DbSet<Student> Students { get; set; }
		public virtual DbSet<SubGroup> SubGroups { get; set; }
		public virtual DbSet<Subject> Subjects { get; set; }
		public virtual DbSet<Teacher> Teachers { get; set; }
		public virtual DbSet<Note> Notes { get; set; }
		public virtual DbSet<News> News { get; set; }

	}
}
