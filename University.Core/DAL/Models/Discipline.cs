﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using University.Core.Entities;

namespace University.Core.DAL.Models
{
	[Table("Disciplines", Schema = "dbo")]
	public class Discipline : BaseEntity
	{
		[Required]
		[Column("Name", TypeName = "nvarchar(200)")]
		[MinLength(3), MaxLength(200)]
		public string Name { get; set; }
		[Column("Description", TypeName = "nvarchar(500)")]
		public string Description { get; set; }
		[Range(1, 20)]
		public int Semester { get; set; }
		public int Year { get; set; }
		public int SpecialtyId { get; set; }
		public int SubjectId { get; set; }
	}
}
