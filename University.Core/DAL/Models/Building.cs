﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using University.Core.Entities;

namespace University.Core.DAL.Models
{
	[Table("Buildings", Schema = "dbo")]
	public class Building : BaseEntity
	{
		[Required]
		[Column("Name", TypeName = "nvarchar(200)")]
		[MinLength(3), MaxLength(200)]
		public string Name { get; set; }
		[Column("Description", TypeName = "nvarchar(500)")]
		public string Description { get; set; }
		[JsonIgnore]
		public ICollection<Auditory> Auditories { get; set; }
	}
}
