﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text;
using University.Core.Entities;
using Newtonsoft.Json;

namespace University.Core.DAL.Models
{
	[Table("Attendances", Schema = "dbo")]
	public class Attendance : BaseEntity
	{
		public bool? CheckIn { get; set; }
		public DateTime Date { get; set; }
		public int ScheduleId { get; set; }
		public int StudentId { get; set; }
		[JsonIgnore]
		public Schedule Schedule { get; set; }
		[JsonIgnore]
		public Student Student { get; set; }
	}
}
