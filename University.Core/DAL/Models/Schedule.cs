﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using University.Core.Entities;

namespace University.Core.DAL.Models
{
	[Table("Schedules", Schema = "dbo")]
	public class Schedule : BaseEntity
	{
		[Range(1, 10)]
		public int LessonNumber { get; set; }
		public DateTime Date { get; set; }
		public TimeSpan Start { get; set; }
		public TimeSpan End { get; set; }
		public int ScheduleTypeId { get; set; }
		public int AuditoryId { get; set; }
		public int SubjectId { get; set; }
		public int TeacherId { get; set; }
		public int GroupId { get; set; }


		public Auditory Auditory { get; set; }
		public ScheduleType ScheduleType { get; set; }
		public Subject Subject { get; set; }
		public Teacher Teacher { get; set; }
		public Group Group { get; set; }
		[JsonIgnore]
		public ICollection<Note> Notes { get; set; }
		[JsonIgnore]
		public ICollection<Attendance> Attendances { get; set; }
	}
}
