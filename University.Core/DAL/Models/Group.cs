﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using University.Core.Entities;

namespace University.Core.DAL.Models
{
	[Table("Groups", Schema = "dbo")]
	public class Group : BaseEntity
	{
		[Required]
		[Column("Name", TypeName = "nvarchar(200)")]
		[MinLength(3), MaxLength(200)]
		public string Name { get; set; }
		[Column("Description", TypeName = "nvarchar(500)")]
		public string Description { get; set; }
		[Required]	
		[Range(0, 20)]
		public int Semester { get; set; }
		public int Year { get; set; }
		public int SpecialtyId { get; set; }
		[JsonIgnore]
		[ForeignKey("SpecialtyId")]
		public Specialty Specialty { get; set; }
		[JsonIgnore]
		public ICollection<Student> Students { get; set; }
		[JsonIgnore]
		public ICollection<SubGroup> SubGroups { get; set; }
		[JsonIgnore]
		public ICollection<WeekSchedule> WeekSchedules { get; set; }
		[JsonIgnore]
		public ICollection<Schedule> Schedules { get; set; }
	}
}
