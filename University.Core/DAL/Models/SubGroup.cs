﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using University.Core.Entities;

namespace University.Core.DAL.Models
{
	[Table("SubGroups", Schema = "dbo")]
	public class SubGroup : BaseEntity
	{
		[Required]
		[Column("Name", TypeName = "nvarchar(200)")]
		[MinLength(3), MaxLength(200)]
		public string Name { get; set; }
		public int GroupId { get; set; }
		[JsonIgnore]
		public Group Group { get; set; }
	}
}
