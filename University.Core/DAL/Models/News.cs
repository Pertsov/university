﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using University.Core.Entities;

namespace University.Core.DAL.Models
{
	public class News : BaseEntity
	{
		[MaxLength(250)]
		public string Author { get; set; }
		[MaxLength(250)]
		public string Title { get; set; }
		public string Text { get; set; }
		public Guid Image { get; set; }
		[JsonIgnore]
		public string ContentType { get; set; }
		public int FacultyId { get; set; }
		[JsonIgnore]
		public string ApplicationUserId { get; set; }
		[JsonIgnore]
		public Faculty Faculty { get; set; }
	}
}
