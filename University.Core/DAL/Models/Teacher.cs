﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using University.Core.Entities;

namespace University.Core.DAL.Models
{
	[Table("Teachers", Schema = "dbo")]
	public class Teacher : BaseEntity
	{
		[Required]
		[Column("Name", TypeName = "nvarchar(500)")]
		[MinLength(10), MaxLength(500)]
		public string Name { get; set; }
		[Required]
		[Column("Description", TypeName = "nvarchar(500)")]
		[MinLength(10), MaxLength(500)]
		public string Description { get; set; }
		[Required]
		[EmailAddress]
		public string Email { get; set; }
		[Phone]
		public string Phone { get; set; }
		public string ApplicationUserId { get; set; }
		[JsonIgnore]
		public ICollection<Schedule> Schedules { get; set; }
	}
}
