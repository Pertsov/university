﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using University.Core.Entities;

namespace University.Core.DAL.Models
{
	[Table("Auditories", Schema = "dbo")]
	public class Auditory : BaseEntity
	{
		public int Amount { get; set; }
		[Required]
		public string Name { get; set; }
		[Column("Description", TypeName = "nvarchar(500)")]
		public string Description { get; set; }
		public int BuildingId { get; set; }
		public int TypeId { get; set; }
		[ForeignKey("TypeId")]
		public AuditoryType AuditoryType { get; set; }
		[ForeignKey("BuildingId")]
		public Building Building { get; set; }
		[JsonIgnore]
		public ICollection<Schedule> Schedules { get; set; }
	}
}
