﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using University.Core.Entities;

namespace University.Core.DAL.Models
{
	public class Note : BaseEntity
	{
		public string Text { get; set; }
		public int ScheduleId { get; set; }
		[Required]
		public string ApplicationUserId { get; set; }
		[JsonIgnore]
		public Schedule Schedule { get; set; }
	}
}
