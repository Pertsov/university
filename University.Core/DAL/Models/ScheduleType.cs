﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using University.Core.Entities;

namespace University.Core.DAL.Models
{
	[Table("ScheduleTypes", Schema = "dbo")]
	public class ScheduleType : BaseEntity
	{
		[Required]
		[Column("Name", TypeName = "nvarchar(200)")]
		[MinLength(3), MaxLength(200)]
		public string Name { get; set; }
		[Column("Description", TypeName = "nvarchar(500)")]
		public string Description { get; set; }
		[MaxLength(50)]
		public string Color { get; set; }
		[JsonIgnore]
		public ICollection<Schedule> Schedules { get; set; }
	}
}
