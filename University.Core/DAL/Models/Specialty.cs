﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using University.Core.Entities;

namespace University.Core.DAL.Models
{
	[Table("Specialties", Schema = "dbo")]
	public class Specialty : BaseEntity
	{
		[Required]
		[Column("Name", TypeName = "nvarchar(200)")]
		[MinLength(3), MaxLength(200)]
		public string Name { get; set; }
		[Column("Description", TypeName = "nvarchar(500)")]
		public string Description { get; set; }
		public int FacultyId { get; set; }
		public Faculty Faculty { get; set; }
		[JsonIgnore]
		public ICollection<Discipline> Disciplines { get; set; }
		[JsonIgnore]
		public ICollection<Group> Groups { get; set; }
	}
}
