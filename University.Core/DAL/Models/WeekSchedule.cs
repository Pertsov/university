﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using University.Core.Entities;

namespace University.Core.DAL.Models
{
	public class WeekSchedule : BaseEntity
	{
		[Range(1, 10)]
		public int Day { get; set; }
		public int LessonNumber { get; set; }
		public int ScheduleTypeId { get; set; }
		public int AuditoryId { get; set; }
		public int SubjectId { get; set; }
		public int TeacherId { get; set; }
		public int GroupId { get; set; }

        public Auditory Auditory { get; set; }
		public ScheduleType ScheduleType { get; set; }
		public Subject Subject { get; set; }
		public Teacher Teacher { get; set; }
		public Group Group { get; set; }
    }
}
