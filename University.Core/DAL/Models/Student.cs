﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using University.Core.Entities;

namespace University.Core.DAL.Models
{
	[Table("Students", Schema = "dbo")]
	public class Student : BaseEntity
	{
		[Required]
		[Column("Name", TypeName = "nvarchar(500)")]
		[MinLength(3), MaxLength(500)]
		public string Name { get; set; }
		public string RecordBook { get; set; }
		public string StudentIdCard { get; set; }
		public string ApplicationUserId { get; set; }
		public int GroupId { get; set; }
		[Required]
		[EmailAddress]
		public string Email { get; set; }
		[Phone]
		public string Phone { get; set; }
		[JsonIgnore]
		public Group Group { get; set; }
		[JsonIgnore]
		public ICollection<Attendance> Attendances { get; set; }
	}
}
