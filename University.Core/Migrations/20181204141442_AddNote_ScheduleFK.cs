﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace University.Core.Migrations
{
    public partial class AddNote_ScheduleFK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ScheduleId",
                table: "Notes",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Notes_ScheduleId",
                table: "Notes",
                column: "ScheduleId");

            migrationBuilder.AddForeignKey(
                name: "FK_Notes_Schedules_ScheduleId",
                table: "Notes",
                column: "ScheduleId",
                principalSchema: "dbo",
                principalTable: "Schedules",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Notes_Schedules_ScheduleId",
                table: "Notes");

            migrationBuilder.DropIndex(
                name: "IX_Notes_ScheduleId",
                table: "Notes");

            migrationBuilder.DropColumn(
                name: "ScheduleId",
                table: "Notes");
        }
    }
}
