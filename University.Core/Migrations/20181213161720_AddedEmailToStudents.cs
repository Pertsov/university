﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace University.Core.Migrations
{
    public partial class AddedEmailToStudents : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Email",
                schema: "dbo",
                table: "Students",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Phone",
                schema: "dbo",
                table: "Students",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Email",
                schema: "dbo",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "Phone",
                schema: "dbo",
                table: "Students");
        }
    }
}
