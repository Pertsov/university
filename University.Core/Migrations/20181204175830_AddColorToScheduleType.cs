﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace University.Core.Migrations
{
    public partial class AddColorToScheduleType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "StudentIdCard",
                schema: "dbo",
                table: "Students",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "RecordBook",
                schema: "dbo",
                table: "Students",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<string>(
                name: "Color",
                schema: "dbo",
                table: "ScheduleTypes",
                maxLength: 50,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Color",
                schema: "dbo",
                table: "ScheduleTypes");

            migrationBuilder.AlterColumn<int>(
                name: "StudentIdCard",
                schema: "dbo",
                table: "Students",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "RecordBook",
                schema: "dbo",
                table: "Students",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
