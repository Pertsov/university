﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace University.Core.Migrations
{
    public partial class AddedImageToNews : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "ApplicationUserId",
                table: "News",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<string>(
                name: "ContentType",
                table: "News",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "Image",
                table: "News",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ContentType",
                table: "News");

            migrationBuilder.DropColumn(
                name: "Image",
                table: "News");

            migrationBuilder.AlterColumn<string>(
                name: "ApplicationUserId",
                table: "News",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
