﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace University.Core.Entities
{
	public class BaseEntity
	{
		[Key]
		[Required]
		public int Id { get; set; }

		[JsonIgnore]
		[Column(TypeName = "nvarchar(100)")]
		public string CreatedBy { get; set; }

		[JsonIgnore]
		public DateTime CreatedOn { get; set; }

		[JsonIgnore]
		[Column(TypeName = "nvarchar(100)")]
		public string ModifiedBy { get; set; }

		[JsonIgnore]
		public DateTime ModifiedOn { get; set; }
	}
}
